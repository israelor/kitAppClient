import React from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";

import { Link, withRouter } from "react-router";

import l20n, { Entity } from "@sketchpixy/rubix/lib/L20n";

import { withFirebase } from "react-redux-firebase";

import {
	Label,
	SidebarBtn,
	NavDropdownHover,
	Navbar,
	Nav,
	NavItem,
	MenuItem,
	Badge,
	Button,
	Icon,
	Grid,
	Row,
	Col
} from "@sketchpixy/rubix";

class Brand extends React.Component {
	render() {
		return (
			<Navbar.Header {...this.props}>
				<Navbar.Brand tabIndex='-1'>
					<a href='#'>
						<h1 style={{ marginTop: "0px" }}>KitApp</h1>
					</a>
				</Navbar.Brand>
			</Navbar.Header>
		);
	}
}

@withRouter
class DirectNavItem extends React.Component {
	render() {
		let active = false;
		let currentLocation = this.props.location.pathname;

		if (!active && this.props.path) {
			active = this.props.router.isActive(this.props.path) && (currentLocation == this.props.path);
		}

		let classes = classNames({
			"pressed": active
		}, this.props.className);

		return (
			<NavItem className={classes} style={this.props.style} href={this.props.path} to={this.props.path} componentClass={Link}>
				<Icon bundle={this.props.bundle || "fontello"} glyph={this.props.glyph} />
			</NavItem>
		);
	}
}

class Skins extends React.Component {
	static skins = ["default", "green", "blue", "purple", "brown", "cyan"];

	switchSkin(skin, e) {
		e.preventDefault();
		e.stopPropagation();
		for (let i = 0; i < Skins.skins.length; i++) {
			$("html").removeClass(Skins.skins[i]);
		}
		$("html").addClass(skin);
		vex.close(this.props.id);
	}

	render() {
		return (
			<Grid style={{ margin: "-2em" }}>
				<Row>
					<Col xs={12} className='text-center bg-darkgrayishblue75' style={{ marginBottom: 25 }}>
						<div className='fg-white' style={{ fontSize: 24, lineHeight: 1, padding: "25px 10px" }}>
							<p>Choose a theme:</p>
						</div>
					</Col>
				</Row>
				<Row>
					<Col xs={4} className='text-center'>
						<a href='#' style={{ border: "none" }} onClick={this.switchSkin.bind(this, "default")}>
							<Icon glyph='icon-fontello-stop icon-4x' className="fg-darkcyan" />
						</a>
					</Col>
					<Col xs={4} className='text-center'>
						<a href='#' style={{ border: "none" }} onClick={this.switchSkin.bind(this, "green")}>
							<Icon glyph='icon-fontello-stop icon-4x' className='fg-darkgreen45' />
						</a>
					</Col>
					<Col xs={4} className='text-center'>
						<a href='#' style={{ border: "none" }} onClick={this.switchSkin.bind(this, "blue")}>
							<Icon glyph='icon-fontello-stop icon-4x' className='fg-blue' />
						</a>
					</Col>
				</Row>
				<Row>
					<Col xs={4} className='text-center'>
						<a href='#' style={{ border: "none" }} onClick={this.switchSkin.bind(this, "purple")}>
							<Icon glyph='icon-fontello-stop icon-4x' className='fg-purple' />
						</a>
					</Col>
					<Col xs={4} className='text-center'>
						<a href='#' style={{ border: "none" }} onClick={this.switchSkin.bind(this, "brown")}>
							<Icon glyph='icon-fontello-stop icon-4x' className='fg-brown' />
						</a>
					</Col>
				</Row>
			</Grid>
		);
	}
}

const flagMenuItems = [
	{ name: "עברית", flag: "Israel", lang: "he" }
];

class FlagMenu extends React.Component {
	state = {
		selectedFlag: "Israel"
	};

	handleSelect = (flag) => {
		this.setState({ selectedFlag: flag }, () => {
			let locale = flagMenuItems.find((item) => {
				return (item.flag === this.state.selectedFlag);
			}).lang;

			if (locale === "he") {
				$("html").addClass("arabic");
			} else {
				$("html").removeClass("arabic");
			}
			l20n.changeLocale(locale);
		});
	}

	render() {
		const flagIcon = (
			<img src={`/imgs/app/flags/flags/flat/32/${this.state.selectedFlag}.png`} width='32' height='32' />
		);

		let menuItems = flagMenuItems.map(({ name, flag, lang }, i) => {
			return (
				<MenuItem key={name} eventKey={flag} active={this.state.selectedFlag === flag}>
					<Grid>
						<Row>
							<Col xs={2}>
								<img src={`/imgs/app/flags/flags/flat/32/${flag}.png`} alt={name} width='32' height='32' />
							</Col>
							<Col xs={10}>
								<Entity className='lang-menu-text' entity='languageMenu' data={{ lang }} defaultValue={name} />
							</Col>
						</Row>
					</Grid>
				</MenuItem>
			);
		});

		menuItems.unshift(
			<MenuItem key='flag-header' header>
				<Entity entity='languageMenuHeading' />
			</MenuItem>
		);

		return (
			<NavDropdownHover noCaret eventKey={2} title={flagIcon} id='flag-menu-btn' className='header-menu' onSelect={this.handleSelect}>
				{menuItems}
			</NavDropdownHover >
		);
	}
}

@withRouter
@withFirebase
class HeaderNavigation extends React.Component {
	handleSkinSwitch = (e) => {
		e.preventDefault();
		e.stopPropagation();
		let vexContent;
		vex.open({
			afterOpen: ($vexContent) => {
				vexContent = $vexContent;
				return ReactDOM.render(<Skins id={$vexContent.data().vex.id} />, $vexContent.get(0));
			},
			afterClose: () => {
				ReactDOM.unmountComponentAtNode(vexContent.get(0));
			}
		});
	}

	handleLogout = (e) => {
		this.props.firebase.logout();
		this.props.router.push(this.getPath("login"));
	}

	getPath = (path) => {
		let dir = this.props.location.pathname.search("rtl") !== -1 ? "rtl" : "ltr";
		path = `/${dir}/${path}`;
		return path;
	}

	render() {
		return (
			<Nav pullRight>
				<Nav className='hidden-xs'>
					<NavItem eventKey={1} href='#' className='hidden-sm' onClick={this.handleSkinSwitch}>
						<Icon glyph='icon-fontello-circle' className='fg-theme' style={{ lineHeight: 1, fontSize: 24, top: 2, position: "relative" }} />
					</NavItem>
					<NavItem divider />
					<FlagMenu />
					<NavItem divider />
				</Nav>
				<Nav>
					<NavItem className='logout' href='#' onClick={this.handleLogout}>
						<Icon bundle='fontello' glyph='off-1' />
					</NavItem>
				</Nav >
			</Nav >
		);
	}
}

export default class Header extends React.Component {
	render() {
		return (
			<Grid id='navbar' {...this.props}>
				<Row>
					<Col xs={12}>
						<Navbar fixedTop fluid id='rubix-nav-header'>
							<Row>
								<Col xs={3} visible='xs'>
									<SidebarBtn />
								</Col>
								<Col xs={6} sm={4}>
									<Brand />
								</Col>
								<Col xs={3} sm={8} collapseRight className='text-right'>
									<HeaderNavigation />
								</Col>
							</Row>
						</Navbar>
					</Col>
				</Row>
			</Grid>
		);
	}
}
