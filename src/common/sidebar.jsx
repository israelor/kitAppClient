import React from "react";
import {Entity} from "@sketchpixy/rubix/lib/L20n";

import {
	Sidebar, SidebarNav, SidebarNavItem, Grid, Row, Col, FormControl,
	Label, Progress, Icon,
	SidebarDivider
} from "@sketchpixy/rubix";

import {Link, withRouter} from "react-router";
import {connect} from "react-redux";
import { withFirebase } from "react-redux-firebase";

@withRouter
@connect(
	({firebase: {auth}}) => ({
		auth
	})
)
@withFirebase
class ApplicationSidebar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isSuperuser: false
		};
	}

	componentDidMount() {
		this.getIsSuperUser();
	}

	handleChange = (e) => {
		this._nav.search(e.target.value);
	}

	getPath = (path) => {
		let dir = this.props.location.pathname.search("rtl") !== -1 ? "rtl" : "ltr";
		path = `/${dir}/${path}`;
		return path;
	}

	logout = async () => {
		await this.props.firebase.logout();
		this.props.router.replace(this.getPath("login"));
	}

	getIsSuperUser = async () => {
		const isSuperuser = await this.props.firebase.ref(`superusers/${this.props.auth.uid}`).once("value");
		this.setState({isSuperuser: isSuperuser.val()});
	}

	render() {
		return (
			<div>
				<Grid>
					<Row>
						<Col xs={12}>
							<FormControl type='text'
								placeholder={"search..."}
								onChange={this.handleChange}
								className='sidebar-search'
								style={{border: "none", background: "none", margin: "10px 0 0 0", borderBottom: "1px solid #666", color: "white"}} />
							<div className='sidebar-nav-container'>
								<SidebarNav style={{marginBottom: 0}} ref={(c) => this._nav = c}>

									{ /** Pages Section */}
									<div className='sidebar-header'><Entity entity='general' /></div>
									{
										this.state.isSuperuser ? 
											<SidebarNavItem glyph='icon-fontello-gauge' name={<Entity entity='dashboard' />} href={this.getPath("dashboard")} />
											:<span/>
									}
									<SidebarNavItem glyph='icon-dripicons-message' name={<Entity entity='kitList' />} href={this.getPath("kits")} />
									<SidebarNavItem glyph='glyphicon-eye-open' name={<Entity entity='viewKit' />} href={this.getPath("viewkit/:id")} />
									<SidebarDivider />

									{ /** user Section */}
									<div className='sidebar-header'>
										<Entity entity='userSettings' />
									</div>
									<SidebarNavItem glyph='glyphicon-log-out' name={<Entity entity='logOut' />} onClick={this.logout} />
								</SidebarNav>
								<br />
								<br />
								<br />
							</div>
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
}

@withRouter
@connect(
	// Map redux state to component props
	({firebase: {auth}}) => ({
		auth
	})
)
export default class SidebarContainer extends React.Component {
	getPath = (path) => {
		let dir = this.props.location.pathname.search("rtl") !== -1 ? "rtl" : "ltr";
		path = `/${dir}/${path}`;
		return path;
	}

	render() {

		if(!this.props.auth.uid) {
			this.props.router.replace(this.getPath("login"));
		}
		return (
			<div id='sidebar'>
				<div id='avatar'>
					<Grid>
						<Row className='fg-white'>
							<Col xs={4} collapseRight>
								<img src={this.props.auth.photoURL} width='40' height='40' />
							</Col>
							<Col xs={8} collapseLeft id='avatar-col'>
								<div style={{top: 23, fontSize: 16, lineHeight: 1, position: "relative"}}>{this.props.auth.displayName}</div>
								<div>
									<Progress id='demo-progress' value={30} color='#ffffff' />
									<Link to={this.getPath("lock")}>
										<Icon id='demo-icon' bundle='fontello' glyph='lock-5' />
									</Link>
								</div>
							</Col>
						</Row>
					</Grid>
				</div>
				<div id='sidebar-container'>
					<Sidebar>
						<ApplicationSidebar />
					</Sidebar>
				</div>
			</div>
		);
	}
}
