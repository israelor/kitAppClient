import React from "react";
import PropTypes from "prop-types";

import {Modal, Button} from "@sketchpixy/rubix";
import ReactLoading from "react-loading";

function ConfirmationDialog({show, onHide, header, text, loading, confirmButton, rejectButton}) {
	return <Modal show={show} onHide={onHide} bsSize="sm" aria-labelledby="contained-modal-title-md">
		<Modal.Header closeButton>
			<Modal.Title id="contained-modal-title-md">{header}</Modal.Title>
		</Modal.Header>
		<Modal.Body>
			{text}
		</Modal.Body>
		<Modal.Footer>
			<Button bsStyle={"red"} onClick={() => onHide(false)}>{rejectButton}</Button>
			<Button bsStyle={"darkcyan"} onClick={() => onHide(true)}>{confirmButton}</Button>
			{loading ? <ReactLoading /> : null}
		</Modal.Footer>
	</Modal>;
}

ConfirmationDialog.propTypes = {
	show: PropTypes.bool.isRequired,
	onHide: PropTypes.func.isRequired,
	text: PropTypes.string,
	header: PropTypes.string,
	confirmButton: PropTypes.string,
	rejectButton: PropTypes.string,
	loading: PropTypes.bool
};

ConfirmationDialog.defaultProps = {
	text: "האם אתה בטוח שברצונך לבצע את הפעולה הבאה?",
	header: "אישור פעולה",
	confirmButton: "אישור",
	rejectButton: "בטל",
	loading: false
};

export default ConfirmationDialog;