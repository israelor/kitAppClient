import l20n from "@sketchpixy/rubix/lib/L20n";

import React from "react";
import {render} from "react-dom";

import {Provider} from "react-redux";

import App from "./app";

import * as localForage from "localforage";

import configureStore from "./redux/configureStore";
import initializeState from "./redux/initializeState";

if (__CLIENT__) {
	const {store, rehydreationPromise} = configureStore(initializeState, localForage); //new CookieStorage(Cookies) until ssr

	rehydreationPromise.then(() => render((<Provider store={store}>
		<App />
	</Provider>), document.getElementById("app-container"), () => {
		l20n.ready();
	}));
}