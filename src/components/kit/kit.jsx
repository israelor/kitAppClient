import React from "react";
import PropTypes from "prop-types";
import { withRouter, Link } from "react-router";

import {
	Panel,
	PanelHeader,
	Grid,
	Row,
	Well,
	Label,
	PanelBody,
	ButtonToolbar,
	Image,
	Icon,
	Button,
	Col,
	PanelFooter,
	PanelContainer
} from "@sketchpixy/rubix";

@withRouter
class Kit extends React.Component {

	constructor(props) {
		super(props);
	}

	getPath = (path) => {
		let dir = this.props.location.pathname.search("rtl") !== -1 ? "rtl" : "ltr";
		path = `/${dir}/${path}`;
		return path;
	};

	render() {
		return (
			<div>
				<div className={"flip-container"}>
					<div className={"flipper"}>
						<div className={"front"}>
							<PanelContainer>
								<Panel>
									<PanelHeader className='bg-darkcyan'>
										<Grid>
											<Row>
												<Col xs={12} className='fg-white'>
													<h4>{this.props.kit.name}
														<Label
															className='fg-white pull-right'>{Object.keys(this.props.kit.users).length}</Label>
													</h4>
												</Col>
											</Row>
										</Grid>
									</PanelHeader>
									<PanelBody style={{ paddingTop: 0, height: "200px" }}>
										<Image style={{ marginRight: "10px" }} width={200} src={this.props.kit.imageUrl}
											thumbnail />
									</PanelBody>
									<PanelFooter className='bg-darkcyan'>
										<Grid>
											<Row>
												<Col xs={12} className='fg-white'>
													<h4></h4>
												</Col>
											</Row>
										</Grid>
									</PanelFooter>
								</Panel>
							</PanelContainer>
						</div>
						<div className={"back"}>
							<PanelContainer>
								<Panel>
									<PanelHeader className='bg-darkcyan'>
										<Grid>
											<Row>
												<Col xs={12} className='fg-white'>
													<h4>{this.props.kit.name}
														<Label
															className='fg-white pull-right'>{this.props.kit.users.length}</Label>
													</h4>
												</Col>
											</Row>
										</Grid>
									</PanelHeader>
									<PanelBody style={{ paddingTop: 0, height: "200px" }}>
										<Well className='kit-description'>
											<ButtonToolbar>
												<p>
													{this.props.kit.description}
												</p>
											</ButtonToolbar>
										</Well>
									</PanelBody>
									<PanelFooter className='bg-darkcyan'>
										<Grid>
											<Row>
												<Col xs={12} className='fg-white'>
													<ButtonToolbar>
														<Button className={"button-flip pull-right"} bsStyle="red">
															<Icon glyph='glyphicon-trash' />
														</Button>
														<Link to={this.getPath(`viewkit/${this.props.kit.name}`)}>
															<Button className={"button-flip pull-left"}>
																<Icon glyph='glyphicon-eye-open' />
															</Button>
														</Link>
													</ButtonToolbar>
												</Col>
											</Row>
										</Grid>
									</PanelFooter>
								</Panel>
							</PanelContainer>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

Kit.propTypes = {
	kit: PropTypes.object.isRequired
};

export default Kit;