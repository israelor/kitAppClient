import React from "react";
import PropTypes from "prop-types";

import {
	Modal,
	Button,
	Form,
	FormGroup,
	InputGroup,
	Icon,
	ControlLabel,
	Col,
	FormControl,
} from "@sketchpixy/rubix";

class SendMessage extends React.Component {

	constructor(props) {
		super(props);
		this.state = { header: "", message: "" };
	}

	onHeaderChanged = (event, value) => {
		this.setState({ header: value });
	};

	onMessageChanged = (event, value) => {
		this.setState({ message: value });
	};

	render() {
		return (
			<Modal {...this.props} bsSize="lg" aria-labelledby="contained-modal-title-md">
				<Modal.Header closeButton>
					<Modal.Title id="contained-modal-title-md">שלח הודעה למשתמש</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form>
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>כותרת</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-mail' />
									</InputGroup.Addon>
									<FormControl value={this.state.header} onChange={this.onHeaderChanged}
										autoFocus type='text' placeholder='הכנס כותרת' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<ControlLabel>תוכן ההודעה</ControlLabel>
							<InputGroup>
								<InputGroup.Addon>
									<Icon glyph='icon-fontello-mail' />
								</InputGroup.Addon>
								<FormControl onChange={this.onMessageChanged}
									value={this.state.message}
									componentClass='textarea' rows='3' placeholder='תוכן ההודעה...' />
							</InputGroup>
						</FormGroup>
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button bsStyle={"red"} onClick={this.props.onHide}>בטל</Button>
					<Button bsStyle={"darkcyan"} onClick={this.props.onHide}>שלח</Button>
				</Modal.Footer>
			</Modal>
		);
	}
}

export default SendMessage;