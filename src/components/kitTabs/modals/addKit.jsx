import React from "react";
import PropTypes from "prop-types";

import {
	Modal,
	Button,
	Form,
	FormGroup,
	InputGroup,
	Image,
	Icon,
	ControlLabel,
	Col,
	FormControl,
} from "@sketchpixy/rubix";

class AddKit extends React.Component {

	constructor(props) {
		super(props);
		this.state = this.emptyState();
	}

	componentDidUpdate() {
		if (this.props.show && !this.map) {
			this.map = new google.maps.Map(document.getElementById("map"), {
				center: { lat: 31.5865, lng: 34.827 },
				zoom: 8
			});
			this.listener = this.map.addListener("click", this.onPointUpdate);
		}
	}

	emptyState() {
		return { name: "", description: "", image: "", latLng: new google.maps.LatLng(0, 0) };
	}

	save = () => {
		const kit = {
			name: this.state.name,
			description: this.state.description,
			imageUrl: this.state.image,
			admins: { [this.props.currentUser.uid]: true },
			users: { [this.props.currentUser.uid]: true },
			latLng: this.state.latLng
		};

		this.props.onSave(kit);

		this.setState(this.emptyState());
	};

	onNameChanged = (event) => {
		this.setState({ name: event.target.value });
	};

	onDescChanged = (event) => {
		this.setState({ description: event.target.value });
	};
	onImageChanged = (event) => {
		this.setState({ image: event.target.value });
	};

	onPointUpdate = (event) => {
		this.setState({ latLng: event.latLng });
		if (this.marker) {
			this.marker.setMap(null);
		}
		this.marker = new google.maps.Marker({ position: event.latLng, map: this.map });
	}

	hide = (event) => {
		this.setState(this.emptyState({}));
		google.maps.event.removeListener(this.listener);
		this.map = null;
		this.props.onHide();
	}

	render() {
		return (
			<Modal onHide={this.props.onHide} show={this.props.show} bsSize="sm" aria-labelledby="contained-modal-title-md">
				<Modal.Header>
					<Modal.Title id="contained-modal-title-md">הוסף קיט</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form>
						{this.state.image !== "" ? <Image width={200} src={this.state.image} thumbnail /> : null}
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>שם קיט</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-mail' />
									</InputGroup.Addon>
									<FormControl value={this.state.name} onChange={this.onNameChanged}
										autoFocus type='text' placeholder='הכנס שם קיט' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>תיאור</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-mail' />
									</InputGroup.Addon>
									<FormControl value={this.state.description} onChange={this.onDescChanged}
										type='text' placeholder='הכנס תיאור' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>תמונת מוצר</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-mail' />
									</InputGroup.Addon>
									<FormControl value={this.state.image} onChange={this.onImageChanged}
										type='text' placeholder='הכנס קישור לתמונת קיט' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<div style={{ width: "100%", height: "10vh" }} id="map"></div>
							</Col>
						</FormGroup>
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button bsStyle={"red"} onClick={this.hide}>בטל</Button>
					<Button bsStyle={"darkcyan"} onClick={this.save}>הוסף</Button>
				</Modal.Footer>
			</Modal>
		);
	}
}

AddKit.propTypes = {
	onHide: PropTypes.func.isRequired,
	onSave: PropTypes.func.isRequired,
	show: PropTypes.bool.isRequired,
	currentUser: PropTypes.object.isRequired
};

export default AddKit;