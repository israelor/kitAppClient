import React from "react";

import {
	Modal,
	Button,
	Form,
	FormGroup,
	InputGroup,
	Icon,
	ControlLabel,
	Col,
	FormControl,
} from "@sketchpixy/rubix";

import ReactLoading from "react-loading";

class AddSongModal extends React.Component {

	constructor(props) {
		super(props);
		this.state = this.emptyState(); 
	}

	emptyState() {
		return { name: "", link: "", artist: "", price: undefined, isLoading: false };
	}

	onNameChanged = (event) => {
		this.setState({ name: event.target.value });
	};

	onLinkChanged = (event) => {
		this.setState({ link: event.target.value });
	};

	onArtistChanged = (event) => {
		this.setState({ artist: event.target.value });
	};

	onPriceChanged = (event) => {
		this.setState({ price: Number(event.target.value)});
	};

	addSong = async () => {
		this.setState({
			isLoading: true
		});
		const newSong = {...this.state};
		delete newSong.isLoading;
		
		await this.props.addSong(newSong);
		
		this.setState(this.emptyState());
	}

	hide = () => {
		this.setState(this.emptyState());
		this.props.onHide();
	}

	render() {
		return (
			<Modal {...this.props} bsSize="lg" aria-labelledby="contained-modal-title-md">
				<Modal.Header closeButton>
					<Modal.Title id="contained-modal-title-md">הוסף שיר לבחירת משתמשים</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form>
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>שיר</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-song' />
									</InputGroup.Addon>
									<FormControl value={this.state.name} onChange={this.onNameChanged}
										autoFocus type='text' placeholder='הכנס שם שיר' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<ControlLabel>קישור</ControlLabel>
							<InputGroup>
								<InputGroup.Addon>
									<Icon glyph='icon-fontello-profile' />
								</InputGroup.Addon>
								<FormControl onChange={this.onArtistChanged}
									value={this.state.artist}
									type='text' placeholder='הכנס אמן' />
							</InputGroup>
						</FormGroup>
						<FormGroup controlId='money'>
							<ControlLabel>מחיר</ControlLabel>
							<InputGroup>
								<InputGroup.Addon>
									<Icon glyph='icon-fontello-link' />
								</InputGroup.Addon>
								<FormControl onChange={this.onPriceChanged}
									value={this.state.price}
									type='number' placeholder='הכנס מחיר' />
							</InputGroup>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<ControlLabel>קישור</ControlLabel>
							<InputGroup>
								<InputGroup.Addon>
									<Icon glyph='icon-fontello-link' />
								</InputGroup.Addon>
								<FormControl onChange={this.onLinkChanged}
									value={this.state.link}
									type='text' placeholder='הכנס קישור' />
							</InputGroup>
						</FormGroup>
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button bsStyle={"red"} onClick={this.hide} disabled={this.state.isLoading}>בטל</Button>
					<Button bsStyle={"darkcyan"} onClick={this.addSong} disabled={this.state.isLoading}>הוסף</Button>
					{this.state.isLoading ? <ReactLoading /> : null}
				</Modal.Footer>
			</Modal>
		);
	}
}

export default AddSongModal;