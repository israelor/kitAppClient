import React from "react";
import PropTypes from "prop-types";

import {
	Modal,
	Button,
	Form,
	FormGroup,
	InputGroup,
	Image,
	Icon,
	ControlLabel,
	Col,
	FormControl,
} from "@sketchpixy/rubix";

import ReactLoading from "react-loading";

class AddProductModal extends React.Component {

	constructor(props) {
		super(props);
		
		this.state = this.emptyState(props.order || {});
	}

	componentWillReceiveProps(nextProps) {
		this.setState(this.emptyState(nextProps.order || {}));
	}

	emptyState(order = {}) {
		return {
			name: order.name || "",
			description: order.description || "",
			price: order.price || "",
			image: order.image || "",
			isLoading: false
		};
	}

	onNameChanged = (event) => {
		this.setState({name: event.target.value});
	};

	onDescriptionChanged = (event) => {
		this.setState({description: event.target.value});
	};
	
	onPriceChanged = (event) => {
		this.setState({price: event.target.value});
	};

	onImageChanged = (event) => {
		this.setState({image: event.target.value});
	};

	onComplete = async () => {
		this.setState({
			isLoading: true
		});
		await this.props.onComplete({...this.state, isLoading: null});
		
		this.setState(this.emptyState());
	};

	hide = () => {
		this.setState(this.emptyState());
		this.props.onHide();
	}

	render() {
		return (
			<Modal {...this.props} bsSize="sm" aria-labelledby="contained-modal-title-md">
				<Modal.Header closeButton>
					<Modal.Title id="contained-modal-title-md">הוסף מוצר</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form>
						{this.state.image !== "" ? <Image width={200} src={this.state.image} thumbnail /> : null}
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>שם מוצר</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-mail' />
									</InputGroup.Addon>
									<FormControl value={this.state.header} onChange={this.onNameChanged}
										autoFocus type='text' placeholder='הכנס שם מוצר' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>תיאור</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-mail' />
									</InputGroup.Addon>
									<FormControl value={this.state.desc} onChange={this.onDescriptionChanged}
										type='text' placeholder='הכנס תיאור מוצר' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>תמונת מוצר</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-mail' />
									</InputGroup.Addon>
									<FormControl value={this.state.image} onChange={this.onImageChanged}
										type='text' placeholder='הכנס קישור לתמונת מוצר' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>מחיר מוצר</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-mail' />
									</InputGroup.Addon>
									<FormControl value={this.state.price} onChange={this.onPriceChanged}
										type='text' placeholder='הכנס מחיר' />
								</InputGroup>
							</Col>
						</FormGroup>
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button bsStyle={"red"} onClick={this.props.onHide}>בטל</Button>
					<Button bsStyle={"darkcyan"} onClick={this.onComplete}>הוסף</Button>
					{ this.state.isLoading ? <ReactLoading /> : null}
				</Modal.Footer>
			</Modal>
		);
	}
}

AddProductModal.propTypes = {
	order: PropTypes.object,
	onHide: PropTypes.func.isRequired,
	onComplete: PropTypes.func.isRequired
};

export default AddProductModal;