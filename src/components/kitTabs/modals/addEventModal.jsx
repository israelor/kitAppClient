import React from "react";

import {
	Modal,
	Button,
	Form,
	FormGroup,
	InputGroup,
	Image,
	Icon,
	ControlLabel,
	Col,
	FormControl,
	Checkbox
} from "@sketchpixy/rubix";

import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";

import ReactLoading from "react-loading";
import { PropTypes } from "prop-types";

class AddEventModal extends React.Component {

	constructor(props) {
		super(props);

		this.state = this.emptyState(props.event || {});
	}

	componentWillReceiveProps(nextProps) {
		this.setState(this.emptyState(nextProps.event || {}));
	}

	emptyState(event = {}) {
		return {
			name: event.name || "",
			description: event.description || "",
			price: event.price || "",
			image: event.image || "",
			date: event.date || moment().unix(),
			timeBeforePurchaseAviable: event.timeBeforePurchaseAviable || 0,
			payBeforehand: event.payBeforehand || false,
			adminApproval: event.adminApproval || false,
			isLoading: false
		};
	}

	onInputChanged = (event) => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};

	onNumberInputChanged = (event) => {
		this.setState({
			[event.target.name]: event.target.value !== "" ? Number(event.target.value) : null
		});
	};

	onInputChecked = (event) => {
		this.setState({
			[event.target.name]: event.target.checked
		});
	};

	onDateChanged = (date) => {
		this.setState({
			date: date.unix()
		});
	}

	onComplete = async () => {
		this.setState({
			isLoading: true
		});
		await this.props.onComplete({ ...this.state, isLoading: null });
		
		this.setState(this.emptyState());
	};

	hide = () => {
		this.setState(this.emptyState());
		this.props.onHide();
	}

	render() {
		return (
			<Modal {...this.props} bsSize="sm" aria-labelledby="contained-modal-title-md">
				<Modal.Header closeButton>
					<Modal.Title id="contained-modal-title-md">הוסף אירוע</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form>
						{this.state.image !== "" ? <Image width={200} src={this.state.image} thumbnail /> : null}
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>שם אירוע</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-mail' />
									</InputGroup.Addon>
									<FormControl value={this.state.name} name="name" onChange={this.onInputChanged}
										autoFocus type='text' placeholder='הכנס שם אירוע' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>תיאור</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-mail' />
									</InputGroup.Addon>
									<FormControl value={this.state.description} name="description" onChange={this.onInputChanged}
										type='text' placeholder='הכנס תיאור אירוע' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>תמונת אירוע</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-picture' />
									</InputGroup.Addon>
									<FormControl value={this.state.image} name="image" onChange={this.onInputChanged}
										type='text' placeholder='הכנס קישור לתמונת אירוע' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>מחיר אירוע</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-shekel' />
									</InputGroup.Addon>
									<FormControl value={this.state.price} name="price" onChange={this.onNumberInputChanged}
										type='number' placeholder='הכנס מחיר' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>תאריך אירוע</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-calender' />
									</InputGroup.Addon>
									<Datetime value={moment.unix(this.state.date)} name="date" onChange={this.onDateChanged}
										placeholder='הכנס תאריך' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<ControlLabel>פתיחת כרטיסים</ControlLabel>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-shekel' />
									</InputGroup.Addon>
									<FormControl value={this.state.timeBeforePurchaseAviable} name="timeBeforePurchaseAviable" onChange={this.onNumberInputChanged}
										type='number' placeholder='הכנס זמן פתיחת כרטיסים מראש בשעות (אם לא תכניס אפשר לקנות כרטיסים ישר)' />
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-shekel' />
									</InputGroup.Addon>
									<Checkbox value={this.state.timeBeforePurchaseAviable} name="adminApproval" onChange={this.onInputChecked}>אישור מנהל</Checkbox>
								</InputGroup>
							</Col>
						</FormGroup>
						<FormGroup controlId='emailaddress'>
							<Col>
								<InputGroup>
									<InputGroup.Addon>
										<Icon glyph='icon-fontello-shekel' />
									</InputGroup.Addon>
									<Checkbox value={this.state.timeBeforePurchaseAviable} name="payBeforehand" onChange={this.onInputChecked}>תשלום מראש</Checkbox>
								</InputGroup>
							</Col>
						</FormGroup>
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button bsStyle={"red"} onClick={this.hide}>בטל</Button>
					<Button bsStyle={"darkcyan"} onClick={this.onComplete}>הוסף</Button>
					{this.state.isLoading ? <ReactLoading /> : null}
				</Modal.Footer>
			</Modal>
		);
	}
}

AddEventModal.propTypes = {
	event: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price:PropTypes.number.isRequired,
		image: PropTypes.string.isRequired,
		date: PropTypes.number.isRequired,
		timeBeforePurchaseAviable: PropTypes.number.isRequired
	}),
	onHide: PropTypes.func.isRequired,
	onComplete: PropTypes.func.isRequired
};

export default AddEventModal;