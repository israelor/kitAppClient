import React from "react";

import {
	Modal,
	Button,
	Form,
	FormGroup,
	ControlLabel,
	FormControl,
} from "@sketchpixy/rubix";

import PropTypes from "prop-types";

class ChangeUserScope extends React.Component {

	constructor(props) {
		super(props);
		this.state = this.emptyState();
	}

	emptyState() {
		return {
			level: 1
		};
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.isAdmin !== nextProps.isAdmin) {
			this.setState({
				level: nextProps.isAdmin ? 2 : 1
			});
		}
	}

	onLevelChanged = (event) => {
		this.setState({
			level: Number(event.target.value)
		});
	}

	changeLevel = async () => {
		const currentIsAdmin = this.state.level === 2;
		if(!currentIsAdmin) {
			await this.props.changeLevel(this.props.user, currentIsAdmin);
			this.setState(this.emptyState());
		} else {
			this.props.onHide();
		}
	}

	hide = () => {
		this.setState(this.emptyState());
		this.props.onHide();
	}

	render() {
		return (
			<Modal {...this.props} bsSize="small" aria-labelledby="contained-modal-title-sm">
				<Modal.Header closeButton>
					<Modal.Title id="contained-modal-title-sm">שנה רמת משתמש</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form>
						<FormGroup controlId="dropdownselect">
							<ControlLabel>בחר רמת ניהול</ControlLabel>
							<FormControl value={this.state.level} onChange={this.onLevelChanged} componentClass="select" placeholder="select">
								<option value='1'>משתמש</option>
								<option value='2'>מנהל</option>
							</FormControl>
						</FormGroup>
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button bsStyle={"red"} onClick={this.hide}>בטל</Button>
					<Button bsStyle={"darkcyan"} onClick={this.changeLevel}>שמור</Button>
				</Modal.Footer>
			</Modal>
		);
	}
}

ChangeUserScope.propTypes = {
	user: PropTypes.object.isRequired,
	isAdmin: PropTypes.bool.isRequired,
	onHide: PropTypes.func.isRequired,
	changeLevel: PropTypes.func.isRequired
};

export default ChangeUserScope;