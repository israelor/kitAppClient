import React from "react";
import PropTypes from "prop-types";
import DjUsers from "./dj/djUsers";
import RequestSongs from "./dj/requestSongs";

import {
	Row,
	Col,
	Grid,
	NavItem,
	Tab,
	Nav,
	PanelBody,
	PanelTabContainer
} from "@sketchpixy/rubix";

import ReactLoading from "react-loading";

import SongList from "./dj/songList";

import { compose } from "redux";
import { firebaseConnect, populate, isLoaded } from "react-redux-firebase";
import { connect } from "react-redux";
import { emptyKitSongs } from "../../models/GroupModel";

const populates = [{
	child: "users", root: "chatusers"
}];

@compose(
	firebaseConnect((props) => [
		{
			path: `groups/kitSongs/${props.kit.name}`,
			storeAs: "kitSongs",
			populates
		}
	]),
	connect(({ firebase }) => {
		const kitSongs = populate(firebase, "kitSongs", populates) || emptyKitSongs;
		if (!kitSongs.users) {
			kitSongs.users = {};
		}
		if (!kitSongs.requests) {
			kitSongs.requests = {};
		}
		if (!kitSongs.songs) {
			kitSongs.songs = {};
		}
		return { kitSongs };
	})
)
class KitDj extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			multiple: false,
		};
	}

	render() {
		if (!isLoaded(this.props.kitSongs)) {
			return <ReactLoading />;
		}
		return (
			<div>
				<h3>הגדרות DJ</h3>
				<PanelTabContainer id='pills-stacked' defaultActiveKey="users" noOverflow>
					<PanelBody>
						<Grid>
							<Row>
								<Col sm={4}>
									<Nav bsStyle="pills" stacked className='tab-darkcyan'>
										<NavItem eventKey="users">רשימת משתמשים</NavItem>
										<NavItem eventKey="songs">רשימת שירים</NavItem>
										<NavItem eventKey="requests">רשימת בקשות</NavItem>
									</Nav>
								</Col>
								<Col sm={8}>
									<Tab.Content>
										<Tab.Pane eventKey="users" className={"dj-tab"}>
											<DjUsers kit={this.props.kit} users={Object.values(this.props.kitSongs.users)} userType="kitSongs" />
										</Tab.Pane>
										<Tab.Pane eventKey="songs" className={"dj-tab"}>
											<SongList songsList={this.props.kitSongs.songs} kitName={this.props.kit.name} />
										</Tab.Pane>
										<Tab.Pane eventKey="requests" className={"dj-tab"}>
											<RequestSongs requests={Object.values(this.props.kitSongs.requests)} />
										</Tab.Pane>
									</Tab.Content>
								</Col>
							</Row>
							<br />
						</Grid>
					</PanelBody>
				</PanelTabContainer>
			</div>
		);
	}
}

KitDj.propTypes = {
	kit: PropTypes.object,
	kitSongs: PropTypes.shape({
		users: PropTypes.object.isRequired,
		requests: PropTypes.object.isRequired,
		songs: PropTypes.object.isRequired
	})
};

export default KitDj;