import React from "react";
import PropTypes from "prop-types";
import DjUsers from "./dj/djUsers";

import {
	Row,
	Col,
	Grid,
	NavItem,
	Tab,
	Nav,
	PanelBody,
	PanelTabContainer
} from "@sketchpixy/rubix";

import ReactLoading from "react-loading";

import ProductList from "./barTender/productsList";
import Purchases from "./barTender/purchases";
import { isLoaded, firebaseConnect, populate } from "react-redux-firebase";
import { compose } from "redux";
import { connect } from "react-redux";
import { emptyKitProduct } from "../../models/GroupModel";

const populates = [{
	child: "users", root: "chatusers"
}];

@compose(
	firebaseConnect((props) => [
		{
			path: `groups/kitProducts/${props.kit.name}`,
			storeAs: "kitProducts",
			populates
		}
	]),
	connect(({ firebase }) => {
		const kitProducts = populate(firebase, "kitProducts", populates) || emptyKitProduct;
		if (!kitProducts.users) {
			kitProducts.users = {};
		}
		if (!kitProducts.orders) {
			kitProducts.orders = {};
		}
		if (!kitProducts.products) {
			kitProducts.products = {};
		}
		return { kitProducts };
	})
)
class BarTender extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		if (!isLoaded(this.props.kitProducts)) {
			return <ReactLoading />;
		}
		return (
			<div>
				<h3>הגדרות ברמן</h3>
				<PanelTabContainer id='pills-stacked' defaultActiveKey="users" noOverflow>
					<PanelBody>
						<Grid>
							<Row>
								<Col sm={4}>
									<Nav bsStyle="pills" stacked className='tab-darkcyan'>
										<NavItem eventKey="users">רשימת משתמשים</NavItem>
										<NavItem eventKey="menu">תפריט מוצרים</NavItem>
										<NavItem eventKey="purchase">רשימת רכישות</NavItem>
									</Nav>
								</Col>
								<Col sm={8}>
									<Tab.Content>
										<Tab.Pane eventKey="users" className={"dj-tab"}>
											<DjUsers kit={this.props.kit} users={Object.values(this.props.kitProducts.users)} userType="kitProducts" />
										</Tab.Pane>
										<Tab.Pane eventKey="menu" className={"dj-tab"}>
											<ProductList products={this.props.kitProducts.products} kitName={this.props.kit.name} />
										</Tab.Pane>
										<Tab.Pane eventKey="purchase" className={"dj-tab"}>
											<Purchases orders={Object.values(this.props.kitProducts.orders)} />
										</Tab.Pane>
									</Tab.Content>
								</Col>
							</Row>
							<br />
						</Grid>
					</PanelBody>
				</PanelTabContainer>
			</div>
		);
	}
}

BarTender.propTypes = {
	kit: PropTypes.object,
	kitProducts: PropTypes.shape({
		user: PropTypes.object.isRequired,
		orders: PropTypes.object.isRequired,
		products: PropTypes.object
	})
};

export default BarTender;