import React from "react";
import PropTypes from "prop-types";

import {
	Row,
	Button,
	Col,
	Grid,
	Icon,
	Form,
	Image,
	Checkbox,
	FormGroup,
	InputGroup,
	FormControl,
	ControlLabel,
} from "@sketchpixy/rubix";
import { withFirebase } from "react-redux-firebase";
import ReactLoading from "react-loading";

@withFirebase
class GeneralSettings extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			kitName: props.kit.name,
			kitDescription: props.kit.description,
			needsMinDistance: props.kit.options ? props.kit.options.needsMinDistance : false,
			minDistance: props.kit.options ? props.kit.options.minDistance : null,
			onNetworkConnect: props.kit.options ? props.kit.options.onNetworkConnect : false,
			networkName: props.kit.options ? props.kit.options.networkName : null,
			adminApproval: props.kit.options ? props.kit.options.adminApproval : false,
			isUpdating: false
		};
	}

	onInputChanged = (event) => {
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	onCheckboxChanged = (event) => {
		this.setState({
			[event.target.name]: event.target.checked
		});
	}

	updateKitSettings = async (e) => {
		e.preventDefault();
		this.setState({
			isUpdating: true
		});
		const promises = [];
		promises.push(this.props.firebase.ref(`groups/data/${this.props.kit.name}`).update({
			description: this.state.kitDescription
		}));
		promises.push(this.props.firebase.ref(`groups/data/${this.props.kit.name}/options`).update({
			needsMinDistance: this.state.needsMinDistance,
			minDistance: +this.state.minDistance || null,
			onNetworkConnect: this.state.onNetworkConnect,
			adminApproval:  this.state.adminApproval,
			networkName:  this.state.networkName || null
		}));
		await Promise.all(promises);
		this.setState({
			isUpdating: false
		});
	}

	render() {
		return (
			<div>
				<h3>הגדרות כלליות</h3>
				<Grid>
					<Row>
						<Col xs={12}>
							<Form onSubmit={this.updateKitSettings}>
								<Col>
									<Image width={100} src={this.props.kit.imageUrl}
										thumbnail />
								</Col>
								<FormGroup controlId='emailaddress'>
									<Col>
										<ControlLabel>שם חדר</ControlLabel>
										<InputGroup>
											<InputGroup.Addon>
												<Icon glyph='icon-fontello-mail' />
											</InputGroup.Addon>
											<FormControl disabled={true} name="kitName" value={this.state.kitName} onChange={this.onInputChanged}
												autoFocus type='text' placeholder='הכנס שם חדר' />
										</InputGroup>
									</Col>
								</FormGroup>
								<FormGroup controlId='emailaddress'>
									<ControlLabel>תיאור</ControlLabel>
									<InputGroup>
										<InputGroup.Addon>
											<Icon glyph='icon-fontello-mail' />
										</InputGroup.Addon>
										<FormControl onChange={this.onInputChanged} name="kitDescription"
											value={this.state.kitDescription} componentClass='textarea'
											rows='2' placeholder='תיאור החדר...' />
									</InputGroup>
								</FormGroup>
								<FormGroup controlId='emailaddress'>
									<ControlLabel>אפשרויות הצטרפות</ControlLabel>
									<Checkbox style={{ display: "flex" }} name='needsMinDistance' checked={this.state.needsMinDistance} onChange={this.onCheckboxChanged}>
										במרחק של {" "}
										<span style={{ float: "left" }}>{" "}קילומטר{" "}</span>
										<input disabled={!this.state.needsMinDistance} className={"input-bottom"} name="minDistance" onChange={this.onInputChanged}
											style={{ maxWidth: "60px", float: "left" }} type='number' value={this.state.minDistance} />
									</Checkbox>
									<Checkbox style={{ display: "flex" }} name='onNetworkConnect' onChange={this.onCheckboxChanged} checked={this.state.onNetworkConnect}>
										בהתחברות לרשת {" "}
										<input disabled={!this.state.onNetworkConnect} className={"input-bottom"} name="networkName" 
											value={this.state.networkName} onChange={this.onInputChanged} style={{ maxWidth: "120px", float: "left" }} type='text' />
									</Checkbox>
									<Checkbox name='adminApproval' checked={this.state.adminApproval}
										onChange={this.onCheckboxChanged} value={this.state.adminApproval}>
										באישור מנהל
									</Checkbox>
									{/*<ButtonToolbar style={{'padding': '15px', float: 'left'}} className={'pull-left'}>*/}
									{/*<Button bsStyle="green">עדכן</Button>*/}
									{/*</ButtonToolbar>*/}
								</FormGroup>
								{this.state.isUpdating ? <ReactLoading className="btn btn-default btn-darkcyan"/> : <Button disabled={this.state.isUpdating} type="submit" bsStyle="darkcyan">עדכן </Button>}
							</Form>
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
}

GeneralSettings.propTypes = {
	kit: PropTypes.object
};

export default GeneralSettings;