import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";

import AddSongModal from "../modals/addSongModal";

import {
	Row,
	Col,
	Grid,
	Table,
	ButtonToolbar,
	Button,
	Icon
} from "@sketchpixy/rubix";
import {withFirebase} from "react-redux-firebase";
import ConfirmationDialog from "../../../common/ConfirmationDialog";

@withFirebase
class SongList extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			showAddSongModal: false,
			showDeleteConfirmation: false,
			songIdToDelete: null,
			deletingSongLoading: false
		};
	}

	componentDidMount() {
		$(ReactDOM.findDOMNode(this.example))
			.addClass("nowrap")
			.dataTable({
				responsive: true,
				"oLanguage": {
					"oPaginate": {
						"sPrevious": "הקודם",
						"sNext": "הבא",
						"sFirst": "דף ראשון",
						"sLast": "דף אחרון",
					},
					"sLengthMenu": "הצג _MENU_ שורות",
					"sInfoEmpty": "אין נתונים בטבלה",
					"sEmptyTable": "אין נתונים בטבלה",
					"sInfo": "סך הכל _TOTAL_ שורות מוצגות (_START_ עד _END_)",
					"sInfoFiltered": " - מסנן מ _MAX_ records",
					"sSearch": "סנן:"

				}
			});
	}

	addSong = async (song) => {
		await this.props.firebase.ref(`groups/kitSongs/${this.props.kitName}/songs`).push(song);
		this.addSongModalClose();
	}

	addSongModalClose = () => this.setState({showAddSongModal: false});

	promptDeleteSong = (songId) => {
		this.setState({
			showDeleteConfirmation: true,
			songIdToDelete: songId
		});
	}

	deleteSong = async (confirmed) => {
		if(confirmed) {
			this.setState({
				deletingSongLoading: true
			});
			await this.props.firebase.ref(`groups/kitSongs/${this.props.kitName}/songs/${this.state.songIdToDelete}`).remove();
		}
		this.setState({
			songIdToDelete: null,
			showDeleteConfirmation: false,
			deletingSongLoading: false
		});
	}

	render() {
		return (
			<div>
				<Grid>
					<Row>
						<Col xs={12}>
							<Table ref={(c) => this.example = c}
								condensed={true}
								hover={true}
								className='display'

								cellSpacing='0' width='100%'>
								<thead>
									<tr>
										<th>שם שיר</th>
										<th>אמן</th>
										<th>מחיר</th>
										<th>קישור</th>
										<th>מחק</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
									</tr>
								</tfoot>
								<tbody>
									{Object.keys(this.props.songsList).map(songId => {
										return (<tr key={songId}>
											<td>{this.props.songsList[songId].name}</td>
											<td>{this.props.songsList[songId].artist}</td>
											<td>{this.props.songsList[songId].price}</td>
											<td><a href={this.props.songsList[songId].link}>קישור</a></td>
											<td>
												<Button onClick={() => this.promptDeleteSong(songId)}><Icon glyph={"glyphicon-trash"} /></Button>
											</td>

										</tr>);
									})}
								</tbody>
							</Table>
						</Col>
					</Row>
				</Grid>
				<div className={"pull-right"}>
					<ButtonToolbar>
						<Button bsStyle='darkcyan'
							rounded
							onClick={() => this.setState({showAddSongModal: true})}>
							<Icon glyph='glyphicon-plus' />
						</Button>
					</ButtonToolbar>
				</div>
				<AddSongModal addSong={this.addSong} show={this.state.showAddSongModal} onHide={this.addSongModalClose} />
				<ConfirmationDialog show={this.state.showDeleteConfirmation} onHide={this.deleteSong} loading={this.state.deletingSongLoading} />
			</div>
		);
	}
}

SongList.propTypes = {
	songsList: PropTypes.objectOf(PropTypes.shape({
		name: PropTypes.string.isRequired,
		artist: PropTypes.string,
		link: PropTypes.string.isRequired,
	})).isRequired,
	kitName: PropTypes.string.isRequired
};

export default SongList;