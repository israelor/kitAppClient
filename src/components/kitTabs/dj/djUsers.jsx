import React from "react";
import PropTypes from "prop-types";

import { Typeahead } from "react-bootstrap-typeahead";

import {
	Col,
	Form,
	Icon,
	Button,
	FormGroup,
	Table,
} from "@sketchpixy/rubix";
import { withFirebase } from "react-redux-firebase";
import ConfirmationDialog from "./../../../common/ConfirmationDialog";

@withFirebase
class DjUsers extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			multiple: true,
			selected: [],
			showDeleteConfirmation: false,
			userToDelete: null,
			userDeleteLoading: false
		};
	}

	pushUser = (arra) => {
		this.setState({ selected: arra });
	};

	addUsers = (event) => {
		event.preventDefault();
		this.props.firebase.update(`groups/${this.props.userType}/${this.props.kit.name}/users`, this.state.selected.reduce((current, user) => {
			current[user.uid] = true;
			return current;
		}, {}));
	}

	promptDeleteUser = (user) => {
		this.setState({
			showDeleteConfirmation: true,
			userToDelete: user
		});
	}

	deleteUser = async (confirmed) => {
		if(confirmed) {
			this.setState({
				userDeleteLoading: true
			});
			await this.props.firebase.ref(`groups/${this.props.userType}/${this.props.kit.name}/users/${this.state.userToDelete.uid}`).remove();
		}
		this.setState({
			userToDelete: null,
			showDeleteConfirmation: false,
			userDeleteLoading: false
		});
	}

	render() {
		return (
			<div>
				<Col xs={12}>
					<Form inline onSubmit={this.addUsers}>
						<FormGroup >
							<Typeahead
								labelKey="displayName"
								options={Object.values(this.props.kit.users)}
								multiple={this.state.multiple}
								onChange={this.pushUser}
								placeholder="בחר משתמש להוספה..."
							/>
						</FormGroup>
						{" "}
						<Button bsStyle={"darkcyan"} type="submit">הוסף</Button>
					</Form>
				</Col>

				<hr />

				<Col xs={12}>
					<Table ref={(c) => this.example = c}
						condensed={true}
						hover={true}
						className='display'
						cellSpacing='0' width='100%'>
						<thead>
							<tr>
								<th>שם משתמש</th>
								<th>פעולה</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
							</tr>
						</tfoot>
						<tbody>
							{this.props.users.map((x) => {
								return (<tr key={x}>
									<td>{x.displayName}</td>
									<td>
										<Button onClick={() => this.promptDeleteUser(x)}><Icon glyph={"glyphicon-trash"} /></Button>
									</td>
								</tr>);
							})}
						</tbody>
					</Table>
				</Col>
				<ConfirmationDialog show={this.state.showDeleteConfirmation} onHide={this.deleteUser} loading={this.state.userDeleteLoading} />
			</div>
		);
	}
}

DjUsers.propTypes = {
	kit: PropTypes.object.isRequired,
	users: PropTypes.array.isRequired,
	userType: PropTypes.string.isRequired
};

export default DjUsers;