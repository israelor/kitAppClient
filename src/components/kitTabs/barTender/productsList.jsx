import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";

import AddProductModal from "../modals/addProductModal";

import {
	Row,
	Col,
	Grid,
	Table,
	ButtonToolbar,
	Button,
	Icon,
} from "@sketchpixy/rubix";

import {withFirebase} from "react-redux-firebase";
import ConfirmationDialog from "./../../../common/ConfirmationDialog";


@withFirebase
class ProductList extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			showAddProductModal: false,
			showDeleteConfirmation: false,
			productIdToDelete: null,
			productDeleteLoading: false
		};
	}

	componentDidMount() {
		$(ReactDOM.findDOMNode(this.example))
			.addClass("nowrap")
			.dataTable({
				responsive: true,
				"oLanguage": {
					"oPaginate": {
						"sPrevious": "הקודם",
						"sNext": "הבא",
						"sFirst": "דף ראשון",
						"sLast": "דף אחרון",
					},
					"sLengthMenu": "הצג _MENU_ שורות",
					"sInfoEmpty": "אין נתונים בטבלה",
					"sEmptyTable": "אין נתונים בטבלה",
					"sInfo": "סך הכל _TOTAL_ שורות מוצגות (_START_ עד _END_)",
					"sInfoFiltered": " - מסנן מ _MAX_ records",
					"sSearch": "סנן:"

				}
			});
	}

	addProductModalClose = () => this.setState({showAddProductModal: false});

	addNewProduct = async (newProduct) => {
		await this.props.firebase.ref(`groups/kitProducts/${this.props.kitName}/products`).push(newProduct);
		this.addProductModalClose();
	}
	
	promptDeleteProduct = (productId) => {
		this.setState({
			showDeleteConfirmation: true,
			productIdToDelete: productId
		});
	}

	deleteProduct = async (confirmed) => {
		if(confirmed) {
			this.setState({
				productDeleteLoading: true
			});
			await this.props.firebase.ref(`groups/kitProducts/${this.props.kitName}/products/${this.state.productIdToDelete}`).remove();
		}
		this.setState({
			productIdToDelete: null,
			showDeleteConfirmation: false,
			productDeleteLoading: false
		});
	}

	render() {
		return (
			<div>
				<Grid>
					<Row>
						<Col xs={12}>
							<Table ref={(c) => this.example = c}
								condensed={true}
								hover={true}
								className='display'
								cellSpacing='0' width='100%'>
								<thead>
									<tr>
										<th>שם מוצר</th>
										<th>תיאור</th>
										<th>תמונה</th>
										<th>מחיר</th>
										<th>פעולה</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
									</tr>
								</tfoot>
								<tbody>
									{Object.keys(this.props.products).map((productId) => {
										return (<tr key={productId}>
											<td>{this.props.products[productId].name}</td>
											<td>{this.props.products[productId].description}</td>
											<td>
												<img src={this.props.products[productId].image} width={50} />
											</td>
											<td>{this.props.products[productId].price + "ש״ח "}</td>
											<td>
												<Button onClick={() => this.promptDeleteProduct(productId)}><Icon glyph={"glyphicon-trash"} /></Button>
											</td>

										</tr>);
									})}
								</tbody>
							</Table>
						</Col>
					</Row>
				</Grid>
				<div className={"pull-right"}>
					<ButtonToolbar>
						<Button bsStyle='darkcyan'
							rounded
							onClick={() => this.setState({showAddProductModal: true})}>
							<Icon glyph='glyphicon-plus' />
						</Button>
					</ButtonToolbar>
				</div>
				<AddProductModal show={this.state.showAddProductModal} onHide={this.addProductModalClose} onComplete={this.addNewProduct} />
				<ConfirmationDialog show={this.state.showDeleteConfirmation} onHide={this.deleteProduct} loading={this.state.productDeleteLoading} />
			</div>
		);
	}
}

ProductList.propTypes = {
	products: PropTypes.object.isRequired,
	kitName: PropTypes.string.isRequired
};

export default ProductList;