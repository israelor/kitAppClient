import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";

import {
	Row,
	Col,
	Grid,
	Table,
	Icon
} from "@sketchpixy/rubix";

class Purchases extends React.Component {

	constructor(props) {
		super(props);
	}

	componentDidMount() {
		$(ReactDOM.findDOMNode(this.example))
			.addClass("nowrap")
			.dataTable({
				responsive: true,
				"oLanguage": {
					"oPaginate": {
						"sPrevious": "הקודם",
						"sNext": "הבא",
						"sFirst": "דף ראשון",
						"sLast": "דף אחרון",
					},
					"sLengthMenu": "הצג _MENU_ שורות",
					"sInfoEmpty": "אין נתונים בטבלה",
					"sEmptyTable": "אין נתונים בטבלה",
					"sInfo": "סך הכל _TOTAL_ שורות מוצגות (_START_ עד _END_)",
					"sInfoFiltered": " - מסנן מ _MAX_ records",
					"sSearch": "סנן:"

				}
			});
	}

	render() {
		return (
			<div>
				<Grid>
					<Row>
						<Col xs={12}>
							<Table ref={(c) => this.example = c}
								condensed={true}
								hover={true}
								className='display'
								cellSpacing='0' width='100%'>
								<thead>
									<tr>
										<th>שם מוצר</th>
										<th>שם קונה</th>
										<th>זמן קנייה</th>
										<th>פעולה</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
									</tr>
								</tfoot>
								<tbody>
									{this.props.orders.map((x, index) => {
										return (<tr key={index}>
											<td>{x.name}</td>
											<td>{x.buyer}</td>
											<td>{x.time}</td>
											<td>
												<Icon glyph={"glyphicon-trash"} />
											</td>

										</tr>);
									})}
								</tbody>
							</Table>
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
}

Purchases.propTypes = {
	orders: PropTypes.array.isRequired
};

export default Purchases;