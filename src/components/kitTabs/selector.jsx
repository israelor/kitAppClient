import React from "react";
import PropTypes from "prop-types";
import DjUsers from "./dj/djUsers";

import {
	Row,
	Col,
	Grid,
	NavItem,
	Tab,
	Nav,
	PanelBody,
	PanelTabContainer
} from "@sketchpixy/rubix";

import ReactLoading from "react-loading";

import Events from "./selector/events";
import Orders from "./selector/orders";

import { compose } from "redux";
import { firebaseConnect, populate, isLoaded } from "react-redux-firebase";
import { connect } from "react-redux";
import { emptyKitEvents } from "./../../models/GroupModel";

const populates = [{
	child: "users", root: "chatusers"
}];

@compose(
	firebaseConnect((props) => [
		{
			path: `groups/kitEvents/${props.kit.name}`,
			storeAs: "kitEvents",
			populates
		}
	]),
	connect(({ firebase }) => {
		const kitEvents = populate(firebase, "kitEvents", populates) || emptyKitEvents;
		if (!kitEvents.users) {
			kitEvents.users = {};
		}
		if (!kitEvents.orders) {
			kitEvents.orders = {};
		}
		if (!kitEvents.events) {
			kitEvents.events = {};
		}
		return { kitEvents };
	})
)
class Selector extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		if (!isLoaded(this.props.kitEvents)) {
			return <ReactLoading />;
		}
		return (
			<div>
				<h3>הגדרות סלקטור</h3>
				<PanelTabContainer id='pills-stacked' defaultActiveKey="users" noOverflow>
					<PanelBody>
						<Grid>
							<Row>
								<Col sm={4}>
									<Nav bsStyle="pills" stacked className='tab-darkcyan'>
										<NavItem eventKey="users">רשימת משתמשים</NavItem>
										<NavItem eventKey="options">אפשרויות אירוע</NavItem>
										<NavItem eventKey="registers">רשימת משתתפים</NavItem>
									</Nav>
								</Col>
								<Col sm={8}>
									<Tab.Content>
										<Tab.Pane eventKey="users" className={"dj-tab"}>
											<DjUsers kit={this.props.kit} users={Object.values(this.props.kitEvents.users)} userType="kitEvents" />
										</Tab.Pane>
										<Tab.Pane eventKey="options" className={"dj-tab"}>
											<Events kitName={this.props.kit.name} events={this.props.kitEvents.events} />
										</Tab.Pane>
										<Tab.Pane eventKey="registers" className={"dj-tab"}>
											<Orders orders={this.props.kitEvents.orders} />
										</Tab.Pane>
									</Tab.Content>
								</Col>
							</Row>
							<br />
						</Grid>
					</PanelBody>
				</PanelTabContainer>
			</div>
		);
	}
}

Selector.propTypes = {
	kitEvents: PropTypes.shape({
		users: PropTypes.object.isRequired,
		events: PropTypes.object.isRequired,
		orders: PropTypes.object.isRequired
	})
};

export default Selector;