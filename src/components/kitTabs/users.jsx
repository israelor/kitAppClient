import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import ChangeUserScope from "../../components/kitTabs/modals/changeUserScope";
import SendMessages from "../../components/kitTabs/modals/sendMessage";

import {
	Row,
	Col,
	Grid,
	Table,
	DropdownButton,
	MenuItem
} from "@sketchpixy/rubix";

import {withFirebase} from "react-redux-firebase";

@withFirebase
class Users extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			showChangUserScope: false,
			showSendMessages: false,
			currentEditUser: {},
			currentEditUserIsAdmin: false
		};
	}

	componentDidMount() {
		$(ReactDOM.findDOMNode(this.example))
			.addClass("nowrap")
			.dataTable({
				responsive: true,
				"oLanguage": {
					"oPaginate": {
						"sPrevious": "הקודם",
						"sNext": "הבא",
						"sFirst": "דף ראשון",
						"sLast": "דף אחרון",
					},
					"sLengthMenu": "הצג _MENU_ שורות",
					"sInfoEmpty": "אין נתונים בטבלה",
					"sEmptyTable": "אין נתונים בטבלה",
					"sInfo": "סך הכל _TOTAL_ שורות מוצגות (_START_ עד _END_)",
					"sInfoFiltered": " - מסנן מ _MAX_ records",
					"sSearch": "סנן:"

				}
			});
	}

	changeScopeClose = () => this.setState({showChangUserScope: false});
	sendMessagesClose = () => this.setState({showSendMessages: false});

	editUser = (user) => {
		this.setState({
			showChangUserScope: true,
			currentEditUser: user,
			currentEditUserIsAdmin: this.props.kit.admins[user.uid]
		});
	}

	changeUserLevel = async (user, isAdmin) => {
		await this.props.firebase.ref(`groups/data/${this.props.kit.name}/admins/${user.uid}`).set(isAdmin ? true : null);
		this.changeScopeClose();
	}

	render() {
		return (
			<div>
				<h3>משתמשי קיט</h3>
				<Grid>
					<Row>
						<Col xs={12}>
							<Table ref={(c) => this.example = c}
								condensed={true}
								hover={true}
								className='display'
								cellSpacing='0' width='100%'>
								<thead>
									<tr>
										<th>שם</th>
										<th>רמת משתמש</th>
										<th>פעולה</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
									</tr>
								</tfoot>
								<tbody>
									{Object.values(this.props.kit.users).map((x, index) => <tr key={index}>
										<td>{x.displayName}</td>
										<td>{this.props.kit.admins[x.uid] ? "מנהל" : "משתמש"}</td>
										<td>
											<DropdownButton title={""} bsStyle='darkcyan' id='dropdown-basic'>
												<MenuItem eventKey="1"
													onClick={() => this.editUser(x)}>
													שנה רמת משתמש
												</MenuItem>
												<MenuItem eventKey="2"
													onClick={() => this.setState({showSendMessages: true})}>שלח הודעה</MenuItem>
											</DropdownButton>
										</td>
									</tr>
									)}
								</tbody>
							</Table>
						</Col>
					</Row>
				</Grid>
				<ChangeUserScope show={this.state.showChangUserScope} onHide={this.changeScopeClose} changeLevel={this.changeUserLevel}
					user={this.state.currentEditUser} isAdmin={this.state.currentEditUserIsAdmin} />
				<SendMessages show={this.state.showSendMessages} onHide={this.sendMessagesClose} />
			</div>
		);
	}
}

Users.propTypes = {
	kit: PropTypes.object.isRequired
};

export default Users;