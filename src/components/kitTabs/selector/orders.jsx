import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";

import {
	Row,
	Col,
	Grid,
	Table,
	ButtonToolbar,
	Button,
	Icon,
} from "@sketchpixy/rubix";

class Orders extends React.Component {

	constructor(props) {
		super(props);
	}

	componentDidMount() {
		$(ReactDOM.findDOMNode(this.example))
			.addClass("nowrap")
			.dataTable({
				responsive: true,
				"oLanguage": {
					"oPaginate": {
						"sPrevious": "הקודם",
						"sNext": "הבא",
						"sFirst": "דף ראשון",
						"sLast": "דף אחרון",
					},
					"sLengthMenu": "הצג _MENU_ שורות",
					"sInfoEmpty": "אין נתונים בטבלה",
					"sEmptyTable": "אין נתונים בטבלה",
					"sInfo": "סך הכל _TOTAL_ שורות מוצגות (_START_ עד _END_)",
					"sInfoFiltered": " - מסנן מ _MAX_ records",
					"sSearch": "סנן:"

				}
			});
	}

	render() {
		return (
			<div>
				<Grid>
					<Row>
						<Col xs={12}>
							<Table ref={(c) => this.example = c}
								condensed={true}
								hover={true}
								className='display'
								cellSpacing='0' width='100%'>
								<thead>
									<tr>
										<th>שם משתמש</th>
										<th>מספר כרטיסים</th>
										<th>סטטוס אישור</th>
										<th>פעולה</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
									</tr>
								</tfoot>
								<tbody>
									{Object.values(this.props.orders).map((x, index) => {
										return (<tr key={index}>
											<td>{x.name}</td>
											<td>{x.event}</td>
											<td>{x.status}</td>
											<td>
												<Icon glyph={"glyphicon-trash"} />
											</td>

										</tr>);
									})}
								</tbody>
							</Table>
						</Col>
					</Row>
				</Grid>
				<div className={"pull-right"}>
					<ButtonToolbar>
						<Button bsStyle='darkcyan'
							rounded
							onClick={() => this.setState({showAddProductModal: true})}>
							<Icon glyph='glyphicon-plus' />
						</Button>
					</ButtonToolbar>
				</div>
			</div>
		);
	}
}

Orders.propTypes = {
	orders: PropTypes.object.isRequired
};

export default Orders;