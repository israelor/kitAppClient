import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";

import AddEventModal from "../modals/addEventModal";

import {
	Row,
	Col,
	Grid,
	Table,
	ButtonToolbar,
	Button,
	Icon
} from "@sketchpixy/rubix";
import {withFirebase} from "react-redux-firebase";
import ConfirmationDialog from "../../../common/ConfirmationDialog";

@withFirebase
class Events extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			showAddEventModal: false,
			showDeleteConfirmation: false,
			eventIdToDelete: null,
			deletingEventLoading: false
		};
	}

	componentDidMount() {
		$(ReactDOM.findDOMNode(this.example))
			.addClass("nowrap")
			.dataTable({
				responsive: true,
				"oLanguage": {
					"oPaginate": {
						"sPrevious": "הקודם",
						"sNext": "הבא",
						"sFirst": "דף ראשון",
						"sLast": "דף אחרון",
					},
					"sLengthMenu": "הצג _MENU_ שורות",
					"sInfoEmpty": "אין נתונים בטבלה",
					"sEmptyTable": "אין נתונים בטבלה",
					"sInfo": "סך הכל _TOTAL_ שורות מוצגות (_START_ עד _END_)",
					"sInfoFiltered": " - מסנן מ _MAX_ records",
					"sSearch": "סנן:"

				}
			});
	}

	addEvent = async (event) => {
		await this.props.firebase.ref(`groups/kitEvents/${this.props.kitName}/events`).push(event);
		this.addEventModalClose();
	}

	addEventModalClose = () => this.setState({showAddEventModal: false});

	promptDeleteEvent = (eventId) => {
		this.setState({
			showDeleteConfirmation: true,
			eventIdToDelete: eventId
		});
	}

	deleteEvent = async (confirmed) => {
		if(confirmed) {
			this.setState({
				deletingEventLoading: true
			});
			await this.props.firebase.ref(`groups/kitEvents/${this.props.kitName}/events/${this.state.eventIdToDelete}`).remove();
		}
		this.setState({
			eventIdToDelete: null,
			showDeleteConfirmation: false,
			deletingEventLoading: false
		});
	}

	render() {
		return (
			<div>
				<Grid>
					<Row>
						<Col xs={12}>
							<Table ref={(c) => this.example = c}
								condensed={true}
								hover={true}
								className='display'

								cellSpacing='0' width='100%'>
								<thead>
									<tr>
										<th>שם אירוע</th>
										<th>תיאור</th>
										<th>תמונה</th>
										<th>תאריך</th>
										<th>מחק</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
									</tr>
								</tfoot>
								<tbody>
									{Object.keys(this.props.events).map(eventId => {
										return (<tr key={eventId}>
											<td>{this.props.events[eventId].name}</td>
											<td>{this.props.events[eventId].description}</td>
											<td><img src={this.props.events[eventId].image} width={50} /></td>
											<td>{moment.unix(this.props.events[eventId].date).toString()}</td>
											<td>
												<Button onClick={() => this.promptDeleteEvent(eventId)}><Icon glyph={"glyphicon-trash"} /></Button>
											</td>

										</tr>);
									})}
								</tbody>
							</Table>
						</Col>
					</Row>
				</Grid>
				<div className={"pull-right"}>
					<ButtonToolbar>
						<Button bsStyle='darkcyan'
							rounded
							onClick={() => this.setState({showAddEventModal: true})}>
							<Icon glyph='glyphicon-plus' />
						</Button>
					</ButtonToolbar>
				</div>
				<AddEventModal onComplete={this.addEvent} show={this.state.showAddEventModal} onHide={this.addEventModalClose} />
				<ConfirmationDialog show={this.state.showDeleteConfirmation} onHide={this.deleteEvent} loading={this.state.deletingEventLoading} />
			</div>
		);
	}
}

Events.propTypes = {
	events: PropTypes.object.isRequired,
	kitName: PropTypes.string.isRequired
};

export default Events;