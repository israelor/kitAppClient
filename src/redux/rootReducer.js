import {
	firebaseReducer
} from "react-redux-firebase";
import {
	persistReducer,
	persistCombineReducers
} from "redux-persist";
import hardSet from "redux-persist/lib/stateReconciler/hardSet";

export default function (storage) {
	const persistConfig = {
		key: "root",
		storage,
		stateReconciler(inboundState, originalState) {
			// Ignore state from cookies, only use preloadedState from window object
			return originalState;
		}
	};

	return persistCombineReducers(persistConfig, {
		firebase: persistReducer({
			key: "firepersist",
			storage,
			stateReconciler: hardSet
		}, firebaseReducer)
	});
}