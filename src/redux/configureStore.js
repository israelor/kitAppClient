import {
	createStore,
	applyMiddleware,
	compose
} from "redux";
import rootReducer from "./rootReducer";
import {
	reactReduxFirebase
} from "react-redux-firebase";

import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

import firebaseConfig from "../firebase/firebase.config";
import {
	persistStore
} from "redux-persist";
import reduxPromiseMiddleware from "redux-promise-middleware";

const middlewares = [reduxPromiseMiddleware()];

const storeEnhancers = [];


if (process.env.NODE_ENV === "development") {

	// If the user has the "Redux DevTools" browser extension installed, use that.
	// Otherwise, hook up the in-page DevTools UI component.
	if (typeof window !== "undefined" && window.devToolsExtension) {
		storeEnhancers.push(window.devToolsExtension());
	}
}

const middlewareEnhancer = applyMiddleware(...middlewares);
storeEnhancers.unshift(middlewareEnhancer);

firebase.initializeApp(firebaseConfig);

// Note: this API requires redux@>=3.1.0
export default function configureStore(initialState, storage) {
	const store = createStore(
		rootReducer(storage),
		initialState,
		compose(reactReduxFirebase(firebase, {}),
			...storeEnhancers)
	);

	if (process.env.NODE_ENV === "development") {
		// Hot reload reducers (requires Webpack or Browserify HMR to be enabled)
		if (module.hot) {
			module.hot.accept("./rootReducer", () =>
				store.replaceReducer(require("./rootReducer").default(storage))
			);
		}
	}

	let rehydrationResolve;
	const rehydreationPromise = new Promise(res => {rehydrationResolve = res;});
	const persistor = persistStore(store, null, () => rehydrationResolve());

	return {
		store,
		persistor,
		rehydreationPromise
	};
}