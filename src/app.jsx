import routes from "./routes";
import l20n from "@sketchpixy/rubix/lib/L20n";

import React from "react";
import { Router, applyRouterMiddleware, browserHistory, hashHistory } from "react-router";


import useScroll from "react-router-scroll/lib/useScroll";
import checkScroll from "@sketchpixy/rubix/lib/node/checkScroll";

import "react-bootstrap-typeahead/css/Typeahead.css";

l20n.initializeLocales({
	"locales": ["he", "enUS"],
	"default": "he"
});


const history = Modernizr.history ? browserHistory : hashHistory;
const App = () => <Router history={history} render={applyRouterMiddleware(useScroll(checkScroll))} routes={routes} />;
export default App;