import "babel-polyfill";
import React from "react";
import {Route} from "react-router";
import Loadable from "react-loadable";

import {Grid, Row, Col, MainContainer} from "@sketchpixy/rubix";
import ReactLoading from "react-loading";

/* Common Components */

import Sidebar from "./common/sidebar";
import Header from "./common/header";
import Footer from "./common/footer";

class App extends React.Component {
	render() {
		return (
			<MainContainer {...this.props}>
				<Sidebar />
				<Header />
				<div id='body'>
					<Grid>
						<Row>
							<Col xs={12}>
								{this.props.children}
							</Col>
						</Row>
					</Grid>
				</div>
				<Footer />
			</MainContainer>
		);
	}
}

/**
 * Includes Sidebar, Header and Footer.
 */
const routes = (
	<Route component={App}>
		<Route path='dashboard' component={Loadable({loader: () => import("./routes/Dashboard"), loading: () => <ReactLoading/>})} />
		<Route path='kits' component={ Loadable({loader: () => import("./routes/Kits"), loading: () => <ReactLoading/>})} />
		<Route path='viewkit/:id' component={Loadable({loader: () => import("./routes/ViewKit"), loading: () => <ReactLoading/>})} />
	</Route>
);

/**
 * No Sidebar, Header or Footer. Only the Body is rendered.
 */
const basicRoutes = (
	<Route>
		<Route path='lock' component={Loadable({loader: () => import("./routes/Lock"), loading: () => <ReactLoading/>})} />
		<Route path='login' component={Loadable({loader: () => import("./routes/Login"), loading: () => <ReactLoading/>})} />
		<Route path='signup' component={Loadable({loader: () => import("./routes/Signup"), loading: () => <ReactLoading/>})} />
	</Route>
);

const combinedRoutes = (
	<Route>
		<Route>
			{routes}
		</Route>
		<Route>
			{basicRoutes}
		</Route>
	</Route>
);

export default (
	<Route>
		<Route path='/ltr'>
			{combinedRoutes}
		</Route>
		<Route path='/rtl'>
			{combinedRoutes}
		</Route>
	</Route>
);
