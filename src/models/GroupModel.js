export const emptyKitProduct = {
	orders: {},
	products: {},
	users: {}
};

export const emptyKitSongs = {
	requests: {},
	songs: {},
	users: {}
};

export const emptyKitEvents = {
	orders: {},
	events: {},
	users: {}
};