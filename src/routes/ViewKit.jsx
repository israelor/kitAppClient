import React from "react";
import { withRouter } from "react-router";
import { Entity } from "@sketchpixy/rubix/lib/L20n";
import {
	PanelTabContainer,
	PanelHeader,
	PanelBody,
	PanelFooter,
	Grid,
	ButtonToolbar,
	Row,
	Tab,
	Button,
	NavItem,
	Nav,
	Col
} from "@sketchpixy/rubix";

import ReactLoading from "react-loading";

import GeneralSetting from "../components/kitTabs/generalSetting";
import UsersKit from "../components/kitTabs/users";
import KitDj from "../components/kitTabs/kitDj";
import BarTender from "../components/kitTabs/barTender";
import Selector from "../components/kitTabs/selector";

import { firebaseConnect, populate, isLoaded } from "react-redux-firebase";
import { connect } from "react-redux";
import { compose } from "redux";

const populates = [{
	child: "users", root: "chatusers"
}];

@compose(
	firebaseConnect((props) => [
		{
			path: `groups/data/${props.params.id}`,
			storeAs: "currentKit",
			populates
		}
	]),
	connect(({ firebase }) => ({
		currentKit: populate(firebase, "currentKit", populates)
	}))
)
@withRouter
export default class ViewKit extends React.Component {
	constructor(props) {
		super(props);
	}

	back = (e) => {
		e.preventDefault();
		e.stopPropagation();
		this.props.router.goBack();
	};

	render() {
		if (!isLoaded(this.props.currentKit)) {
			return <ReactLoading />;
		}
		return (
			<PanelTabContainer id='tabs-basic' defaultActiveKey="general">
				<PanelHeader className='bg-darkcyan fg-white'>
					<Grid>
						<Row>
							<Col sm={12}>
								<h3>{this.props.currentKit.name}</h3>
							</Col>
						</Row>
					</Grid>
				</PanelHeader>
				<PanelBody>
					<Grid>
						<Row>
							<Col sm={12}>
								<Nav bsStyle="tabs" className='tab-darkcyan'>
									<NavItem eventKey="general">
										<Entity entity={"general"} />
									</NavItem>
									<NavItem eventKey="kitUsers">
										<Entity entity={"kitUsers"} />
									</NavItem>
									<NavItem eventKey="djSettings">
										<Entity entity={"djSettings"} />
									</NavItem>
									<NavItem eventKey="barTenderSettings">
										<Entity entity={"barTenderSettings"} />
									</NavItem>
									<NavItem eventKey="selectorSettings">
										<Entity entity={"selectorSettings"} />
									</NavItem>
								</Nav>
								<Tab.Content>
									<Tab.Pane eventKey="general">
										<GeneralSetting kit={this.props.currentKit} />
									</Tab.Pane>
									<Tab.Pane eventKey="kitUsers">
										<UsersKit kit={this.props.currentKit} />
									</Tab.Pane>
									<Tab.Pane eventKey="djSettings">
										<KitDj kit={this.props.currentKit} />
									</Tab.Pane>
									<Tab.Pane eventKey="barTenderSettings">
										<BarTender kit={this.props.currentKit} />
									</Tab.Pane>
									<Tab.Pane eventKey="selectorSettings">
										<Selector kit={this.props.currentKit} />
									</Tab.Pane>
								</Tab.Content>
							</Col>
						</Row>
					</Grid>
				</PanelBody>
				<PanelFooter className='bg-darkcyan fg-white'>
					<Grid>
						<Row>
							<Col sm={12} lg={12} md={12}>
								<ButtonToolbar style={{ "padding": "15px", float: "left" }} className={"pull-left"}>
									<Button bsStyle="red" onClick={this.back}>חזור</Button>
								</ButtonToolbar>
							</Col>
						</Row>
					</Grid>
				</PanelFooter>
			</PanelTabContainer>
		);
	}
}