import React from "react";
import {withRouter} from "react-router";

import Kit from "../components/kit/kit";
import AddKit from "../components/kitTabs/modals/addKit";

import {
	ButtonToolbar,
	Icon,
	Button,
} from "@sketchpixy/rubix";

import ReactLoading from "react-loading";

import { firebaseConnect, isLoaded } from "react-redux-firebase";
import { connect } from "react-redux";
import { compose } from "redux";
import GeoFire from "geofire";

@compose(
	firebaseConnect((props, store) => [
		{
			path: "groups/data",
			queryParams: [
				"parsed",
				`orderByChild=admins/${store.getState().firebase.auth.uid}`,
				"equalTo=true"
			],
			storeAs: "kits"
		}
	]),
	connect(({firebase}) => ({
		kits: firebase.data.kits || [],
		auth: firebase.auth
	}))
)
@withRouter
export default class Kits extends React.Component {

	constructor(props) {
		super(props);
		this.state = { showAddKitModal: false};
	}

	componentDidMount() {
		setTimeout(() => {
		}, 5000);
	}

	addKit = async (kit) => {
		const location = kit.latLng;
		delete kit.latLng;
		await this.props.firebase.ref(`groups/data/${kit.name}`).set(kit);
		if(!this.geoGroups) {
			this.geoGroups = new GeoFire(this.props.firebase.ref("groups/locations"));	
		}
		await this.geoGroups.set(kit.name, [location.lat(), location.lng()]);
		this.addKitClose();
	};

	addKitClose = () => {
		this.setState({showAddKitModal: false});
	};

	render() {
		const kit = {
			width: "220px",
			float: "right",
			marginLeft: "20px",
			marginTop: "18px"
		};
		if(!isLoaded(this.props.kits)) {
			return <ReactLoading />;
		}
		return (
			<div>
				{Object.values(this.props.kits).map(x => {
					return (
						<div style={kit} key={Math.random()}>
							<Kit kit={x} />
						</div>
					);
				})}
				<div style={{position: "fixed", bottom: "50px", left: "25px"}}>
					<ButtonToolbar>
						<Button bsStyle='darkcyan' rounded onClick={() => this.setState({showAddKitModal: true})}>
							<Icon glyph='glyphicon-plus' />
						</Button>
					</ButtonToolbar>
				</div>

				<AddKit onSave={this.addKit} onHide={this.addKitClose} show={this.state.showAddKitModal} currentUser={this.props.auth} />
			</div>
		);
	}
}
