import React from "react";
import classNames from "classnames";
import { Link, withRouter } from "react-router";
import { withFirebase } from "react-redux-firebase";

import {
	Row,
	Col,
	Icon,
	Grid,
	Form,
	Badge,
	Panel,
	Button,
	PanelBody,
	FormGroup,
	LoremIpsum,
	InputGroup,
	FormControl,
	ButtonGroup,
	ButtonToolbar,
	PanelContainer,
} from "@sketchpixy/rubix";
import { connect } from "react-redux";

@withRouter
@withFirebase
@connect(
	// Map redux state to component props
	({ firebase: { auth } }) => ({
		auth
	})
)
export default class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			email: "",
			password: ""
		};
	}

	back = (e) => {
		e.preventDefault();
		e.stopPropagation();
		this.props.router.goBack();
	}

	updatePassword = (e) => {
		this.setState({
			password: e.target.value
		});
	}

	updateEmail = (e) => {
		this.setState({
			email: e.target.value
		});
	}

	login = async (e) => {
		e.preventDefault();
		try {
			await this.props.firebase.login({
				email: this.state.email,
				password: this.state.password
			});
			this.props.router.push(this.getPath("kits"));
		} catch (err) {
			console.error(err);

		}
	}

	facebookLogin = async (e) => {
		try {
			await this.props.firebase.login({
				provider: "facebook",
				type: "popup"
			});

			this.props.router.push(this.getPath("kits"));
		} catch (err) {
			console.error(err);
		}
	}

	twitterLogin = async (e) => {
		try {
			await this.props.firebase.login({
				provider: "twitter",
				type: "popup"
			});

			this.props.router.push(this.getPath("kits"));
		} catch (err) {
			console.error(err);
		}
	}

	componentDidMount() {
		$("html").addClass("authentication");
		if (this.props.auth.isLoaded && !this.props.auth.isEmpty) {
			this.props.router.push(this.getPath("kits"));
		}
	}

	componentWillUnmount() {
		$("html").removeClass("authentication");
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.auth.isLoaded && !nextProps.auth.isEmpty) {
			this.props.router.push(this.getPath("kits"));
			return false;
		}
	}

	getPath = (path) => {
		var dir = this.props.location.pathname.search("rtl") !== -1 ? "rtl" : "ltr";
		path = `/${dir}/${path}`;
		return path;
	}

	render() {
		return (
			<div id='auth-container' className='login'>
				<p>test</p>
				<div id='auth-row'>
					<div id='auth-cell'>
						<Grid>
							<Row>
								<Col sm={4} smOffset={4} xs={10} xsOffset={1} collapseLeft collapseRight>
									<PanelContainer controls={false}>
										<Panel>
											<PanelBody style={{ padding: 0 }}>
												<div className='text-center bg-darkblue fg-white'>
													<h3 style={{ margin: 0, padding: 25 }}>Sign in to Rubix</h3>
												</div>
												<div className='bg-hoverblue fg-black50 text-center' style={{ padding: 12.5 }}>
													<div>You need to sign in for those awesome features</div>
													<div style={{ marginTop: 12.5, marginBottom: 12.5 }}>
														<Button id='facebook-btn' lg bsStyle='darkblue' type='submit' onClick={this.facebookLogin}>
															<Icon glyph='icon-fontello-facebook' />
															<span>Sign in <span className='hidden-xs'>with facebook</span></span>
														</Button>
													</div>
													<div>
														<a id='twitter-link' style={{cursor: "pointer"}} onClick={this.twitterLogin}><Icon glyph='icon-fontello-twitter' /><span> or with twitter</span></a>
													</div>
												</div>
												<div>
													<div className='text-center' style={{ padding: 12.5 }}>
														or use your Rubix account
													</div>
													<div style={{ padding: 25, paddingTop: 0, paddingBottom: 0, margin: "auto", marginBottom: 25, marginTop: 25 }}>
														<Form onSubmit={this.login}>
															<FormGroup controlId='emailaddress'>
																<InputGroup bsSize='large'>
																	<InputGroup.Addon>
																		<Icon glyph='icon-fontello-mail' />
																	</InputGroup.Addon>
																	<FormControl autoFocus type='email' value={this.state.email} className='border-focus-blue' onChange={this.updateEmail} placeholder='support@sketchpixy.com' />
																</InputGroup>
															</FormGroup>
															<FormGroup controlId='password'>
																<InputGroup bsSize='large'>
																	<InputGroup.Addon>
																		<Icon glyph='icon-fontello-key' />
																	</InputGroup.Addon>
																	<FormControl type='password' value={this.state.password} className='border-focus-blue' onChange={this.updatePassword} placeholder='password' />
																</InputGroup>
															</FormGroup>
															<FormGroup>
																<Grid>
																	<Row>
																		<Col xs={6} collapseLeft collapseRight style={{ paddingTop: 10 }}>
																			<Link to={this.getPath("signup")}>Create a Rubix account</Link>
																		</Col>
																		<Col xs={6} collapseLeft collapseRight className='text-right'>
																			<Button outlined lg type='submit' bsStyle='blue'>Login</Button>
																		</Col>
																	</Row>
																</Grid>
															</FormGroup>
														</Form>
													</div>
												</div>
											</PanelBody>
										</Panel>
									</PanelContainer>
								</Col>
							</Row>
						</Grid >
					</div >
				</div >
			</div >
		);
	}
}
