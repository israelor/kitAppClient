import React from "react";
import classNames from "classnames";
import {Link, withRouter} from "react-router";

import {
	Row,
	Col,
	Icon,
	Grid,
	Form,
	Badge,
	Panel,
	Button,
	PanelBody,
	FormGroup,
	LoremIpsum,
	InputGroup,
	FormControl,
	ButtonGroup,
	ButtonToolbar,
	PanelContainer,
} from "@sketchpixy/rubix";
import {withFirebase} from "../../node_modules/react-redux-firebase";

@withRouter
@withFirebase
export default class Signup extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			username: "",
			email: "",
			password: ""
		};
	}

	back = (e) => {
		e.preventDefault();
		e.stopPropagation();
		this.props.router.goBack();
	}

	componentDidMount() {
		$("html").addClass("authentication");
	}

	componentWillUnmount() {
		$("html").removeClass("authentication");
	}

	handleInputChange = (event) => {
		const target = event.target;
		const name = target.name;

		this.setState({
			[name]: target.value
		});
	}

	signup = async (e) => {
		e.preventDefault();
		try {
			await this.props.firebase.createUser({
				email: this.state.email,
				username: this.state.username,
				password: this.state.password
			});
			this.props.router.push(this.getPath("kits"));
		} catch (err) {
			alert(err.message);
			console.error(err);
		}
	}


	getPath = (path) => {
		var dir = this.props.location.pathname.search("rtl") !== -1 ? "rtl" : "ltr";
		path = `/${dir}/${path}`;
		return path;
	}

	render() {
		return (
			<div id='auth-container' className='login'>
				<div id='auth-row'>
					<div id='auth-cell'>
						<Grid>
							<Row>
								<Col sm={4} smOffset={4} xs={10} xsOffset={1} collapseLeft collapseRight>
									<PanelContainer controls={false}>
										<Panel>
											<PanelBody style={{padding: 0}}>
												<div className='text-center bg-darkblue fg-white'>
													<h3 style={{margin: 0, padding: 25}}>Sign up</h3>
												</div>
												<div>
													<div style={{padding: 25, paddingTop: 0, paddingBottom: 0, margin: "auto", marginBottom: 25, marginTop: 25}}>
														<Form onSubmit={this.signup}>
															<FormGroup controlId='username'>
																<InputGroup bsSize='large'>
																	<InputGroup.Addon>
																		<Icon glyph='icon-fontello-user' />
																	</InputGroup.Addon>
																	<FormControl autoFocus type='text' className='border-focus-blue' placeholder='Username' name="username" onChange={this.handleInputChange} />
																</InputGroup>
															</FormGroup>
															<FormGroup controlId='emailaddress'>
																<InputGroup bsSize='large'>
																	<InputGroup.Addon>
																		<Icon glyph='icon-fontello-mail' />
																	</InputGroup.Addon>
																	<FormControl type='email' className='border-focus-blue' placeholder='support@sketchpixy.com' name="email" onChange={this.handleInputChange} />
																</InputGroup>
															</FormGroup>
															<FormGroup controlId='password'>
																<InputGroup bsSize='large'>
																	<InputGroup.Addon>
																		<Icon glyph='icon-fontello-key' />
																	</InputGroup.Addon>
																	<FormControl type='password' className='border-focus-blue' placeholder='password' name="password" onChange={this.handleInputChange} />
																</InputGroup>
															</FormGroup>
															<FormGroup>
																<Grid>
																	<Row>
																		<Col xs={12} collapseLeft collapseRight>
																			<Button type='submit' outlined lg bsStyle='blue'>Create account</Button>
																		</Col>
																	</Row>
																</Grid>
															</FormGroup>
														</Form>
													</div>
													<div className='bg-hoverblue fg-black50 text-center' style={{padding: 25, paddingTop: 12.5}}>
														<div style={{marginTop: 25}}>
															Already have an account? <Link to={this.getPath("login")}>Login</Link>
														</div>
													</div>
												</div>
											</PanelBody>
										</Panel>
									</PanelContainer>
								</Col>
							</Row>
						</Grid>
					</div >
				</div >
			</div >
		);
	}
}
