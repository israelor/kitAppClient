import React from "react";

import {
	Row,
	Tab,
	Col,
	Nav,
	Icon,
	Grid,
	Table,
	Label,
	Panel,
	Button,
	NavItem,
	Progress,
	PanelBody,
	PanelLeft,
	LoremIpsum,
	PanelRight,
	PanelHeader,
	PanelContainer,
	PanelTabContainer,
} from "@sketchpixy/rubix";
import { compose } from "redux";
import { firebaseConnect } from "react-redux-firebase";
import { connect } from "react-redux";
import ReactLoading from "react-loading";

@compose(
	firebaseConnect(() => [
		{ path: "chatusers" }
	]),
	connect(({ firebase }) => ({
		chatUsers: firebase.data.chatusers
	}))
)
class MaleFemaleChart extends React.Component {
	constructor(props) {
		super(props);
		this.state = props.chatUsers ? this.graphData(props.chatUsers) : { males: [], females: [] };
	}

	componentWillReceiveProps(nextProps) {
		this.setState(nextProps.chatUsers ? this.graphData(nextProps.chatUsers) : { males: [], females: [] });
	}

	graphData(chatUsers) {
		if (!chatUsers) {
			return { males: [], females: [] };
		}
		const mappedYear = Object.values(chatUsers).reduce((prev, user) => {
			if (user.createDate && user.gender) {
				const date = new moment(user.createDate).format("DD-MM-YYYY");
				if (!prev[date]) {
					prev[date] = { male: 0, female: 0 };
				}
				prev[date][user.gender.toLowerCase()]++;
			}
			return prev;
		}, {});

		const females = [];
		const males = [];

		Object.entries(mappedYear).forEach(([date, { male, female }]) => {
			if (male > 0) {
				males.push({ x: date, y: male });
			}
			females.push({ x: date, y: female });
		});
		return { males, females };
	}

	componentDidMount() {
		this.updateComponent();
	}

	componentWillUpdate() {
		this.updateComponent();
	}

	updateComponent() {
		if (!this.chart) {
			this.chart = new Rubix("#male-female-chart", {
				height: 200,
				title: "פילוח דמוגרפי",
				subtitle: "מבקרים",
				axis: {
					male: {
						type: "ordinal",
						tickFormat: "d",
						tickCount: 2,
						label: "תאריך הצטרפות"
					},
					female: {
						type: "linear",
						tickFormat: "d"
					}
				},
				tooltip: {
					theme_style: "dark",
					format: {
						y: ".0f"
					},
					abs: {
						y: true
					}
				},
				stacked: true,
				interpolate: "linear",
				show_markers: true
			});


			this.femalesColumn = this.chart.column_series({
				name: "משתמשים ממין נקבה",
				color: "#FF0097",
				marker: "cross"
			});

			this.malesColumn = this.chart.column_series({
				name: "משתמשים ממין זכר",
				color: "#2D89EF",
				marker: "diamond"
			});

			this.femalesColumn.addData(this.state.females);

			this.malesColumn.addData(this.state.males);
		} else {
			this.femalesColumn.update(this.state.females);

			this.malesColumn.update(this.state.males);
		}
	}

	render() {
		return <div id='male-female-chart'></div>;
	}
}

@compose(
	firebaseConnect(() => [
		{ path: "groups/data", storeAs: "allKits" }
	]),
	connect(({ firebase }) => ({
		kits: firebase.data.allKits
	}))
)
class RevenuePanel extends React.Component {
	countNewKits() {
		return Object.values(this.props.kits).filter((kit) =>
			kit.createDate && moment(kit.createDate).format("DD-MM-YYYY") === moment().format("DD-MM-YYYY") // does kit created today
		).length;
	}

	render() {
		if (!this.props.kits) {
			return <ReactLoading />;
		}

		const kitCount = Object.keys(this.props.kits).length;
		const newKitsCount = this.countNewKits();

		return <Grid>
			<Row>
				<Col xs={12} className='text-center'>
					<br />
					<div>
						<h4>סך קיטים במערכת</h4>
						<h2 className='fg-green visible-xs visible-md visible-lg'>{kitCount}</h2>
						<h4 className='fg-green visible-sm'>{kitCount}</h4>
					</div>
					<hr className='border-green' />
					<div>
						<h4>קיטים חדשים</h4>
						<h2 className='fg-green visible-xs visible-md visible-lg'>{newKitsCount}</h2>
						<h4 className='fg-green visible-sm'>{newKitsCount}</h4>
					</div>
				</Col>
			</Row>
		</Grid>;
	}
}

class LoadPanel extends React.Component {
	render() {
		return (
			<Row className='bg-green fg-lightgreen'>
				<Col xs={6}>
					<h3>גידול יומי</h3>
				</Col>
				<Col xs={6} className='text-right'>
					<h2 className='fg-lightgreen'>67%</h2>
				</Col>
			</Row>
		);
	}
}

class AlertChart extends React.Component {
	componentDidMount() {
		let chart = new Rubix("#alert-chart", {
			width: "100%",
			height: 200,
			hideLegend: true,
			hideAxisAndGrid: true,
			focusLineColor: "#fff",
			theme_style: "dark",
			axis: {
				x: {
					type: "linear"
				},
				y: {
					type: "linear",
					tickFormat: "d"
				}
			},
			tooltip: {
				color: "#fff",
				format: {
					x: "d",
					y: "d"
				}
			},
			margin: {
				left: 25,
				top: 50,
				right: 25,
				bottom: 25
			}
		});

		let alerts = chart.column_series({
			name: "משתמשים",
			color: "#7CD5BA",
			nostroke: true
		});

		alerts.addData([
			{ x: 0, y: 30 },
			{ x: 1, y: 40 },
			{ x: 2, y: 15 },
			{ x: 3, y: 30 },
			{ x: 4, y: 35 },
			{ x: 5, y: 70 },
			{ x: 6, y: 50 },
			{ x: 7, y: 60 },
			{ x: 8, y: 35 },
			{ x: 9, y: 30 },
			{ x: 10, y: 40 },
			{ x: 11, y: 30 },
			{ x: 12, y: 50 },
			{ x: 13, y: 35 }
		]);
	}

	render() {
		return (
			<Row>
				<Col xs={12}>
					<div id='alert-chart' className='rubix-chart'></div>
				</Col>
			</Row>
		);
	}
}


@compose(
	firebaseConnect(() => [
		{ path: "chatusers" }
	]),
	connect(({ firebase }) => ({
		chatUsers: firebase.data.chatusers ? Object.values(firebase.data.chatusers) : []
	}))
)
class RadarChartPanel extends React.Component {
	graphData(chatUsers) {
		if (!chatUsers) {
			return {
				countries: [],
				males: [],
				females: []
			};
		}
		const countriesToIndex = {};
		const usersByCountry = [];
		const countries = Array.from(new Set(chatUsers.map((user) => {
			if (!countriesToIndex[user.country]) {
				usersByCountry.push({ male: 0, female: 0 });
				countriesToIndex[user.country] = usersByCountry.length - 1;
			}
			usersByCountry[countriesToIndex[user.country]][user.gender ? user.gender.toLowerCase() : "male"]++;
			return user.country;
		})));
		return {
			countries,
			males: usersByCountry.map(country => country.male),
			females: usersByCountry.map(country => country.female)
		};
	}

	componentDidMount() {
		const usersData = this.graphData(this.props.chatUsers);
		let data = {
			labels: usersData.countries,
			datasets: [{
				label: "משתמש ממין זכר",
				fillColor: "rgba(220,220,220,0.2)",
				strokeColor: "rgba(220,220,220,1)",
				pointColor: "rgba(220,220,220,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data: usersData.males
			}, {
				label: "משתמש ממין נקבה",
				fillColor: "rgba(234, 120, 130, 0.5)",
				strokeColor: "rgba(234, 120, 130, 1)",
				pointColor: "rgba(234, 120, 130, 1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(151,187,205,1)",
				data: usersData.females
			}]
		};

		let ctx = document.getElementById("chartjs-1").getContext("2d");
		new Chart(ctx).Radar(data, {
			responsive: false,
			maintainAspectRatio: true
		});

		$(".line-EA7882").sparkline("html", {
			type: "line",
			height: 25,
			lineColor: "#EA7882",
			fillColor: "rgba(234, 120, 130, 0.5)",
			sparkBarColor: "#EA7882"
		});
		$(".line-2EB398").sparkline("html", {
			type: "line",
			height: 25,
			lineColor: "#2EB398",
			fillColor: "rgba(46, 179, 152, 0.5)",
			sparkBarColor: "#2EB398"
		});
		$(".line-79B0EC").sparkline("html", {
			type: "line",
			height: 25,
			lineColor: "#79B0EC",
			fillColor: "rgba(121, 176, 236, 0.5)",
			sparkBarColor: "#79B0EC"
		});
		$(".line-FFC497").sparkline("html", {
			type: "line",
			height: 25,
			lineColor: "#FFC497",
			fillColor: "rgba(255, 196, 151, 0.5)",
			sparkBarColor: "#FFC497"
		});
	}

	render() {
		return (
			<div>
				<canvas id='chartjs-1' height='250' width='250'></canvas>
			</div>
		);
	}
}

class OrdersComparisonPanel extends React.Component {
	componentDidMount() {
		let chart = new Rubix("#orderscomparision", {
			height: 225,
			noSort: true,
			hideYAxis: true,
			title: "פילוח על פי קיט פרימיום",
			subtitle: "",
			hideXAxisTickLines: true,
			hideYAxisTickLines: true,
			hideLegend: true,
			gridColor: "#EBEBEB",
			tickColor: "#EBA068",
			titleColor: "#EBA068",
			subtitleColor: "#EBA068",
			axis: {
				x: {
					type: "ordinal"
				},
				y: {
					type: "linear",
					tickFormat: "d"
				}
			},
			margin: {
				top: 50
			},
			tooltip: {
				color: "#EBA068",
				format: {
					y: ".0f"
				}
			},
			show_markers: false
		});

		let series1 = chart.column_series({
			name: "קיט פרימיום",
			color: "#EBA068",
			marker: "square",
			fillopacity: 1
		});

		series1.addData([
			{ x: "Sun", y: 1 },
			{ x: "Mon", y: 2 },
			{ x: "Tue", y: 3 },
			{ x: "Wed", y: 2 },
			{ x: "Thu", y: 2 },
			{ x: "Fri", y: 3 },
			{ x: "Sat", y: 1 }
		]);


		let series2 = chart.column_series({
			name: "קיט רגיל",
			color: "#FFD3B1",
			fillopacity: 1
		});

		series2.addData([
			{ x: "Sun", y: 3 },
			{ x: "Mon", y: 4 },
			{ x: "Tue", y: 6 },
			{ x: "Wed", y: 5 },
			{ x: "Thu", y: 5.5 },
			{ x: "Fri", y: 3 },
			{ x: "Sat", y: 2 }
		]);

		$(".compositebar1").sparkline("html", { type: "bar", barColor: "#ffffff", height: 25 });
	}

	render() {
		return (
			<div>
				<div id='orderscomparision'></div>
				<Grid style={{ marginTop: 0 }}>
					<Row className='bg-lightorange fg-darkorange text-center'>
						<Col xs={12} collapseLeft collapseRight style={{ paddingTop: 0 }}>
							<Table alignMiddle collapsed>
								<tbody>
									<tr>
										<td style={{ width: "33%" }}>
											<h6>סך קיטים פרימיום</h6>
											<h4>160</h4>
										</td>
										<td style={{ width: "33%" }}>
											<div style={{ position: "relative" }}>
												<div className='compositebar1'>4,6,7,7,4,3,2,1,4,9,3,2,3,5,2,4,3,1</div>
											</div>
										</td>
										<td style={{ width: "33%" }}>
											<h4>+ 1%</h4>
										</td>
									</tr>
									<tr>
										<td style={{ width: "33%" }}>
											<h6>סך קיטים רגילים</h6>
											<h4>2,312</h4>
										</td>
										<td style={{ width: "33%" }}>
											<div style={{ position: "relative" }}>
												<div className='compositebar1'>3,2,4,6,3,6,7,3,2,1,5,7,8,9,3,2,6,7</div>
											</div>
										</td>
										<td style={{ width: "33%" }}>
											<h4>+ 12%</h4>
										</td>
									</tr>
								</tbody>
							</Table>
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
}

@compose(
	firebaseConnect(() => [
		{ path: "groups/data", storeAs: "allKits" }
	]),
	connect(({ firebase }) => ({
		kits: firebase.data.allKits ? Object.values(firebase.data.allKits) : []
	}))
)
class TicketsPanel extends React.Component {
	getRandomColor() {
		var letters = "0123456789ABCDEF";
		var color = "#";
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}

	componentWillUpdate() {
		this.ticketsCleared.addData(this.kitsToData().data);
	}

	componentDidMount() {
		this.ticketsCleared = Rubix.Donut("#tickets-cleared", {
			title: "קיטים פופולאריים",
			subtitle: "על פי מספר משתמשים",
			titleColor: "#EBA068",
			subtitleColor: "#EBA068",
			hideLegend: false,
			height: 300,
			tooltip: {
				color: "#EBA068"
			}
		});

		this.ticketsCleared.addData(this.kitsToData().data);
	}

	kitsToData() {
		if (!this.props.kits) {
			return { data: [], max: 0 };
		}
		let max = 0;
		const data = this.props.kits.map(kit => {
			const usersCount = Object.keys(kit.users).length;
			if (usersCount > max) {
				max = usersCount;
			}
			return {
				name: kit.name,
				value: usersCount,
				color: this.getRandomColor()
			};
		});
		return { data: data.sort((a, b) => (a.value < b.value ? 1 : -1)).slice(0, 10), max };
	}

	render() {
		if (!this.props.kits) {
			return <ReactLoading />;
		}
		const { data, max } = this.kitsToData();
		return (
			<div>
				<div id='tickets-cleared'></div>
				<Table collapsed>
					<tbody>
						{
							data.map((kit) => <tr>
								<td style={{ padding: "12.5px 25px" }}>
									<Progress label={kit.name} value={kit.value} color={kit.color} min={0} max={max} />
								</td>
								<td style={{ padding: "12.5px 25px" }} className='text-right'>
									<Label>{kit.value}</Label>
								</td>
							</tr>)
						}
					</tbody>
				</Table>
			</div>
		);
	}
}

export default class Dashboard extends React.Component {
	render() {
		return <div className='dashboard'>
			<Row>
				<Col sm={12}>
					<PanelTabContainer id='dashboard-main' defaultActiveKey="demographics">
						<Panel horizontal className='force-collapse'>
							<PanelLeft className='bg-red fg-white panel-sm-1'>
								<Nav bsStyle="tabs" className='plain'>
									<NavItem eventKey="demographics">
										<Icon bundle='fontello' glyph='chart-bar-5' />
									</NavItem>
								</Nav>
							</PanelLeft>
							<PanelBody className='panel-sm-4' style={{ padding: 0 }}>
								<Grid>
									<Row>
										<Col xs={12} collapseLeft collapseRight>
											<Tab.Content>
												<Tab.Pane eventKey="demographics">
													<MaleFemaleChart />
												</Tab.Pane>
											</Tab.Content>
										</Col>
									</Row>
								</Grid>
							</PanelBody>
							<PanelRight className='bg-lightgreen fg-white panel-sm-2'>
								<RevenuePanel />
							</PanelRight>
							<PanelRight className='bg-green fg-green panel-sm-4'>
								<Grid>
									<LoadPanel />
									<AlertChart />
								</Grid>
							</PanelRight>
						</Panel>
					</PanelTabContainer>
				</Col>
			</Row>

			<Row>
				<Col sm={6} collapseRight>
					<PanelContainer>
						<Panel>
							<PanelBody style={{ padding: 0 }}>
								<Grid>
									<Row>
										<Col xs={12} className='text-center' style={{ padding: 25 }}>
											<RadarChartPanel />
										</Col>
									</Row>
								</Grid>
							</PanelBody>
						</Panel>
					</PanelContainer>
				</Col>
				<Col sm={6}>
					<PanelTabContainer id='dashboard-contacts-sales-tickets' defaultActiveKey='sales'>
						<Panel>
							<PanelHeader className='bg-lightorange fg-darkorange fg-tab-active'>
								<Nav bsStyle="tabs">
									<NavItem eventKey="sales">
										<Icon className='icon-1-and-quarter-x' bundle='feather'
											glyph='bar-graph-2' />
									</NavItem>
									<NavItem eventKey="tickets">
										<Icon className='icon-1-and-quarter-x' bundle='feather' glyph='pie-graph' />
									</NavItem>
								</Nav>
							</PanelHeader>
							<PanelBody style={{ paddingTop: 0 }}>
								<Tab.Content>
									<Tab.Pane eventKey='sales'>
										<OrdersComparisonPanel />
									</Tab.Pane>
									<Tab.Pane eventKey='tickets'>
										<TicketsPanel />
									</Tab.Pane>
								</Tab.Content>
							</PanelBody>
						</Panel>
					</PanelTabContainer>
				</Col>
			</Row>
		</div>;
	}
}
