import path from "path";
import express from "express";
import compression from "compression";
import cookieParser from "cookie-parser";

// import React from "react";
// import ReactDOMServer from "react-dom/server";

// import {AppContainer} from "react-hot-loader";
// import {Provider} from "react-redux";

// import routes from "./src/routes";
// import { match, RouterContext } from "react-router";
import RubixAssetMiddleware from "@sketchpixy/rubix/lib/node/RubixAssetMiddleware";
// import { renderHTMLString } from "@sketchpixy/rubix/lib/node/router";

import Cookies from "cookies";
// import configureStore from "./src/redux/configureStore";
// import initalizeState from "./src/redux/initializeState";
// import { getStoredState } from "redux-persist";
// import { CookieStorage, NodeCookiesWrapper } from "redux-persist-cookie-storage";

// global.XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

const port = process.env.PORT || 8080;

let app = express();
app.use(compression());
app.use(Cookies.express());
app.use(cookieParser());
app.use(express.static(path.join(process.cwd(), "public")));
app.set("views", path.join(process.cwd(), "views"));
app.set("view engine", "pug");

function renderHTML(req, res) {
	res.render("index");
}

/** BEGIN X-EDITABLE ROUTES */

app.get("/xeditable/groups", function(req, res) {
	res.send([
		{value: 0, text: "Guest"},
		{value: 1, text: "Service"},
		{value: 2, text: "Customer"},
		{value: 3, text: "Operator"},
		{value: 4, text: "Support"},
		{value: 5, text: "Admin"}
	]);
});

app.get("/xeditable/status", function(req, res) {
	res.status(500).end();
});

app.post("/xeditable/address", function(req, res) {
	res.status(200).end();
});

app.post("/dropzone/file-upload", function(req, res) {
	res.status(200).end();
});

/** END X-EDITABLE ROUTES */

app.post("/dropzone/file-upload", function(req, res) {
	res.status(200).end();
});

app.get("/", (req, res, next) => {
	res.redirect("rtl/kits");
});

app.get("/ltr/*", RubixAssetMiddleware("ltr"), (req, res, next) => {
	renderHTML(req, res);
});

app.get("/rtl/*", RubixAssetMiddleware("rtl"), (req, res, next) => {
	renderHTML(req, res);
});

app.listen(port, () => {
	console.log(`Node.js app is running at http://localhost:${port}/`);
});
