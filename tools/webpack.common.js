module.exports = {
	mode: process.env.NODE_ENV || "development",
	module: {
		noParse: /node_modules\/localforage\/dist\/localforage.js/,
		rules: [
			{
				test: /\.js$/,
				use: ["source-map-loader"],
				enforce: "pre"
			},
			{
				test: /\.jsx?$/,
				exclude: /node_modules|bower_components/,
				loader: "babel-loader"
			},
			{
				test: /\.txt$/,
				exclude: /(node_modules|bower_components)/,
				loader: "raw-loader"
			},
			{
				test: /\.(css|scss})$/,
				loader: "style-loader!css-loader"
			},
			{
				test: /\.(ico|gif|png|jpg|jpeg|svg|webp)$/,
				use: ["file-loader?context=public&name=/[path][name].[ext]"],
				exclude: /node_modules/
			},
			{
				test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url-loader?limit=10000&mimetype=application/font-woff&name=./fonts/[hash].[ext]"
			},
			{
				test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url-loader?limit=10000&mimetype=application/font-woff&name=./fonts/[hash].[ext]"
			},
			{
				test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url-loader?limit=10000&mimetype=application/octet-stream&name=./fonts/[hash].[ext]"
			},
			{
				test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
				loader: "file-loader&name=./fonts/[hash].[ext]"
			},
			{
				test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url-loader?limit=10000&mimetype=image/svg+xml&name=./fonts/[hash].[ext]"
			},
		]
	},
	resolve: {
		modules: [
			"src",
			"sass",
			"public",
			"node_modules"
		],
		extensions: [".json", ".js", ".jsx"]
	}
};