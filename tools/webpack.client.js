var path = require("path");
var rtlcss = require("rtlcss");
var webpack = require("webpack");
var deepmerge = require("deepmerge");
var autoprefixer = require("autoprefixer");
var webpackCommonConfig = require("./webpack.common");
var MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
var CaseSensitivePathsPlugin = require("case-sensitive-paths-webpack-plugin");

var isProduction = process.env.NODE_ENV === "production";

var sourceMap = false;

if (process.env.SOURCEMAP === "true") {
	sourceMap = true;
}

var wds = {
	hostname: process.env.WP_HOST || "localhost",
	port: process.env.WP_PORT || 8079
};

var wdsPath = "http://" + wds.hostname + ":" + wds.port;
var publicPath = wdsPath + "/assets/";

var devtool = "";
var entry = {
	"app": ["./src/main.jsx"],
	"main": ["./sass/main.scss"],
	"main-rtl": ["./sass/main.rtl.scss"],
	"plugins": ["./src/plugins.js"]
};

var plugins = [
	new webpack.DefinePlugin({
		__CLIENT__: true,
		__SERVER__: false,
		__PRODUCTION__: isProduction,
		__DEV__: !isProduction,
		"process.env.NODE_ENV": "\"" + process.env.NODE_ENV + "\"",
		__DEVTOOLS__: true
	}),
	new CaseSensitivePathsPlugin()
];

if (process.env.EXTRACT_TEXT_PLUGIN === "true") {
	plugins.unshift(new MiniCssExtractPlugin({
		filename: "css/[name].css"
	}));
}

if (isProduction) {
	plugins.unshift(new webpack.optimize.OccurrenceOrderPlugin());
	plugins.unshift(new BundleAnalyzerPlugin({
		analyzerMode: "static",
		openAnalyzer: true
	}));
} else {
	plugins.unshift(new webpack.HotModuleReplacementPlugin());
}

function getStyleLoader(prefixer) {
	var s = "";

	if (sourceMap) s = "sourceMap";

	if (process.env.EXTRACT_TEXT_PLUGIN === "false") {
		return [
			"style-loader",
			"css-loader?-minimize&importLoaders=1&root=../public&" + s,
			{
				loader: "postcss-loader?pack=" + prefixer + "!sass-loader?" + s
			}
		];
	}

	return [
		MiniCssExtractPlugin.loader,
		"css-loader?-minimize&importLoaders=1&" + s,
		{
			loader: "postcss-loader?pack=" + prefixer + "&" + s,
			options: {
				plugins: () => {
					if(!process.env.RTL) {
						return [autoprefixer({
							browsers: ["last 2 versions", "> 1%", "ie 9"]
						})];
					} else {
						return [
							autoprefixer({
								browsers: ["last 2 versions", "> 1%", "ie 9"]
							}),
							rtlcss
						];
					}
				},
			}
		},
		"sass-loader?" + s
	];
}

devtool = sourceMap ? "source-map" : "";

if (!isProduction) {
	for (var key in entry) {
		if (entry.hasOwnProperty(key)) {
			entry[key].push("webpack/hot/only-dev-server");
		}
	}

	entry.devServerClient = "webpack-dev-server/client?" + wdsPath;
}

var ltrloaders = getStyleLoader("normalprefixer");
var rtlloaders = getStyleLoader("rtlprefixer");

if (process.env.RTL !== "true") {
	rtlloaders = ["null-loader"];
}

var rules = webpackCommonConfig.module.rules.concat();
// ltr/rtl loaders
rules.push({
	test: function (absPath) {
		if (absPath.search("rtl.scss") !== -1) {
			return true;
		}
		return false;
	},
	use: rtlloaders
});
rules.push({
	test: function (absPath) {
		if (absPath.search("rtl.scss") === -1 &&
			absPath.search(".scss") !== -1) {
			return true;
		}
		return false;
	},
	use: ltrloaders
});

// script loader for plugins.js
var pluginLoaders = ["script-loader"];
if (isProduction) {
	pluginLoaders.push("uglify-loader");
}
rules.push({
	test: /(\/|\\)public(\/|\\)(.*?)\.js$/,
	use: pluginLoaders
});

delete webpackCommonConfig.module;

module.exports = deepmerge({
	devtool: devtool,
	entry: entry,
	module: {
		rules: rules
	},
	devServer: {
		proxy: {
			"*": wdsPath
		},
		publicPath: publicPath,
		hot: true,
		inline: false,
		lazy: false,
		headers: {
			"Access-Control-Allow-Origin": "*"
		},
		stats: {
			colors: true
		},
		host: wds.hostname,
		port: wds.port
	},
	output: {
		path: path.join(process.cwd(), "public"),
		publicPath: isProduction ? "/" : publicPath,
		chunkFilename: "js/[name].js",
		filename: "js/[name].js",
	},
	externals: [
		{
			moment: "moment"
		}
	],
	plugins: plugins,
}, webpackCommonConfig);