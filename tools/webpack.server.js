var fs = require("fs");
var path = require("path");
var webpack = require("webpack");
var deepmerge = require("deepmerge");
var webpackCommonConfig = require("./webpack.common");
var CaseSensitivePathsPlugin = require("case-sensitive-paths-webpack-plugin");

var nodeModules = {};
fs.readdirSync("node_modules")
	.filter(function (x) {
		return [".bin"].indexOf(x) === -1;
	})
	.forEach(function (mod) {
		nodeModules[mod] = "commonjs " + mod;
	});

var sourceMapSupportModule = "require('source-map-support').install({environment: 'node'});\n\n";

var output = {
	path: path.join(process.cwd(), "tmp"),
	filename: "bundle.js"
};

if (process.env.NO_OUTPUT_PATH) {
	output = {
		path: process.cwd(),
		filename: "./server.js"
	};
}

var rules = webpackCommonConfig.module.rules.concat();
rules.push({
	test: /\.scss$/,
	loader: "null-loader"
});

delete webpackCommonConfig.module;

module.exports = deepmerge({
	devtool: "source-map",
	entry: [
		"./server.babel.js"
	],
	output: output,
	target: "node",
	module: {
		rules: rules
	},
	plugins: [
		new webpack.BannerPlugin({
			banner: sourceMapSupportModule,
			raw: true,
			entryOnly: true
		}),
		new CaseSensitivePathsPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.DefinePlugin({
			__CLIENT__: false,
			__SERVER__: true,
			__PRODUCTION__: false,
			__DEV__: true,
			"process.env.NODE_ENV": "\"" + process.env.NODE_ENV + "\""
		})
	],
	externals: nodeModules
}, webpackCommonConfig);